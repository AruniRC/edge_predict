%% Run fine-tuning for classification
%   Starting with a network pre-trained on the gradient edge-prediction
%   task, we fine-tune it for the classification task.


%% Settings

rng(0);
expDir = 'data/ft_edge_gradient_cub_v1';

% base (pre-trained) network path
opts.baseNetwork = 'data/edge_gradient_v3/cnn_model/net_occlusion.mat';


%% TODO ---
%   Get the IMDB

opts.dataset = 'cub';

if ~exist(fullfile(expDir, 'imdb', 'imdb-cnn.mat'), 'file')
    cubDir = 'data/CUB_200_2011/CUB_200_2011';
    imdb = cub_get_database(cubDir, false, true);
    vl_xmkdir(fullfile(expDir, 'imdb'));
    save(fullfile(expDir, 'imdb', 'imdb-cnn.mat'), '-struct', 'imdb');
else
    imdb = load(fullfile(expDir, 'imdb', 'imdb-cnn.mat'));
end


% specify which dataset is to be used (e.g. CUB, cars etc.)


%% Fine-tune the network

% stage 1: update weights for only the first and the last layers
opts.ftStage = 'ft-classifier-stage-1';
[net, info] = cnn_edge_train(...
                      'baseNetwork', opts.baseNetwork, ...
                      'expDir', fullfile(expDir, 'cnn-train'), ...
                      'batchNormalization', false, ...
                      'imdbPath', fullfile(expDir, 'imdb', 'imdb-cnn.mat'), ...
                      'dataset', opts.dataset, ...
                      'networkType', 'dagnn', ...
                      'modelType', opts.ftStage, ...
                      'quantizeInputs', false, ...    % use real-valued inputs
                      'quantizeOutput', false, ...
                      'train', struct('gpus', []));

% save the network
if ~exist(fullfile(expDir, 'cnn_model', 'net_edge.mat'), 'file')
    vl_xmkdir(fullfile(expDir, 'cnn_model')); s = saveobj(net);
    save(fullfile(expDir, 'cnn_model', 'net-classifier-stage-1.mat'), '-struct', 's');
end

