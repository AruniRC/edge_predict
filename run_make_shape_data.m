% Script to create the random-shapes dataset
%   -- random rectangles

% settings
outDir = '/data/arunirc/datasets/rectangle_v1/';
DEBUG = false; % set to `true` for looking at 20 images
visDir = 'data/rectangle_v1/vis-output';  % output visualizations of histograms
numData = 100000;
imSize = 100;

vl_xmkdir(visDir);
vl_xmkdir(outDir);


%% Make dataset
%   - random rectangles - v1

rng(0); % better be reproducible!

% train set
vl_xmkdir(fullfile(outDir, 'train'));
parfor ii = 1:numData
    disp(ii);
    im = get_rect_image(100, 100);
    imwrite(im, fullfile(outDir, 'train', [num2str(ii) '.png']));
end

% val set
vl_xmkdir(fullfile(outDir, 'val'));
parfor ii = 1:1000
    disp(ii);
    im = get_rect_image(100, 100);
    imwrite(im, fullfile(outDir, 'val', [num2str(ii) '.png']));
end


%% Make IMDB

imdb = edge_get_imdb(outDir);


%% Plot histogram values

% sample images from training set
rng(0);
batch = train(randperm(length(train), 1000));

opts.numGpus = 0;
opts.visDir = visDir; 
opts.debug = true;
opts.quantizeOutput = false;
opts.doAbs = true;

inputs = getRectangleEdgeBatch(opts, imdb, batch);
inputResponse = inputs{2};
outputResponse = inputs{4};

resVec = outputResponse(:);

% show the histogram of target response values
figure; histogram(resVec);
xlim([0 0.5]); title('Hist(response)');
hold on;

% show the quantiles
percBins = prctile(resVec, [25 50 75]);
plot(percBins, [0 0 0], 'r*')

disp(percBins)

