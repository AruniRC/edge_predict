%--------------------------------------------------------------------------
function save_alexnet_response(net, imgDir, imgNames, outDir, cellSize)
%--------------------------------------------------------------------------
% imgDir = '/data/arunirc/datasets/ImageNet/images';
% cellSize = 8;

% net = cnn_imagenet_deploy(net);
net.move('gpu');

for ii = 1:length(imgNames)
    fprintf('Image %d of %d: %s\n', ii, length(imgNames), imgNames{ii});
    try
        info = imfinfo(fullfile(imgDir,imgNames{ii}));

        if ~isequal(info.Format, 'jpg')
            % skip non-JPG images, like the CMYK in ImageNet
            continue;
        end   

        img = imread(fullfile(imgDir,imgNames{ii}));
    catch ME
       ME
       continue; 
    end    
    
    x = get_cnn_filter_response(net, img);
    
    [setDir,imName, ext] = fileparts(imgNames{ii});
    vl_xmkdir(fullfile(outDir,setDir));
    tmp_save(fullfile(outDir,setDir,imName), x);
end


%--------------------------------------------------------------------------
function tmp_save(outPath, x) 
%--------------------------------------------------------------------------
save(outPath, 'x');

