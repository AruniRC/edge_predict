% Script to create the random-shapes dataset
%   -- random rectangles

% settings
outDir_1 = '/data/arunirc/datasets/edge_lines_imagenet_v1_binary/';
outDir_2 = '/data/arunirc/datasets/edge_lines_imagenet_v1_energy/';

visDir =  'data/edge_lines_imagenet_v1_vis';
DEBUG = true; % set to `true` for looking at 20 images
imSize = 200;

vl_xmkdir(outDir_1);
vl_xmkdir(outDir_2);
vl_xmkdir(visDir);


%% Make dataset
%   - random rectangles - v1

rng(0); % better be reproducible!


% the 100k images from ImageNet
imagenetDir = '/data/arunirc/datasets/ImageNet/images/';
imdbImagenet = edge_get_imdb(imagenetDir);


%% 
    
if DEBUG
    rng(0);
    
    fn = imdbImagenet.images.name(randperm(10000,20));

    figure;
    for ii = 1:length(fn)
        
        try
            im = imread(fullfile(imdbImagenet.imageDir, fn{ii}));   
            
        catch ME
           ME
           continue; 
        end 
            
        im = imresize(im, [imSize imSize]);

        if size(im,3) > 1
            im = rgb2gray(im);
        end  

        edgeIm = edge(im,'Canny',0.0005,1.5);    % canny edges - binary output   

        [Gmag,~] = imgradient(im, 'sobel');      % sobel gradients   
        Gmag = Gmag ./ (max(Gmag(:)) + eps) ; 
        edgeIm2 = Gmag .* edgeIm ;

        vl_tightsubplot(1,3,1); imagesc(im); axis('square');
        vl_tightsubplot(1,3,2); imagesc(edgeIm); axis('square'); % colormap('gray');
        vl_tightsubplot(1,3,3); imagesc(edgeIm2); axis('square'); % colormap('gray');

        drawnow;

        pause(2);


        saveas(gca, fullfile(visDir, [num2str(ii) '.png']));

        % imwrite(edgeIm, fullfile(visDir, imdbImagenet.images.name{ii}));
    
    end
end


%% Create the entire dataset

fn = imdbImagenet.images.name;
parfor ii = 1:length(fn)
    disp(ii);
    
    im = imread(fullfile(imdbImagenet.imageDir, fn{ii}));   
    im = imresize(im, [imSize imSize]);
    
    if size(im,3) > 1
        im = rgb2gray(im);
    end  
    
    edgeIm = edge(im,'Canny',0.0005,1.5);    % canny edges - binary output   
    [Gmag,~] = imgradient(im, 'sobel');      % sobel gradients   
    Gmag = Gmag ./ (max(Gmag(:)) + eps) ; 
    edgeIm2 = Gmag .* edgeIm ;
    
    % make directories and all that!
    [a,b,c] = fileparts(imdbImagenet.images.name{ii});
    vl_xmkdir(fullfile(outDir, a));
    
    % save the binary edge image
    imwrite(edgeIm, fullfile(outDir, imdbImagenet.images.name{ii}));
    
end



