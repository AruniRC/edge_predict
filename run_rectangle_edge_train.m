
%% Initial settings

expDir = 'data/edge_rect_v1.3.1';
dataDir = '/data/arunirc/datasets/rectangle_v1/';
rng(0);


%% Make IMDB

if ~exist(fullfile(expDir, 'imdb', 'imdb-cnn.mat'), 'file')  
    imdb = edge_get_imdb(dataDir);
    vl_xmkdir(fullfile(expDir, 'imdb'));
    
    % modify the messy train file names
    imdb.imageDir = fullfile(dataDir);
    trainFn = imdb.images.name(imdb.images.set==1);
    a = cellfun(@(x)(strsplit(x, '/')), trainFn, 'un', 0); % get rid of first folder in path
    b = cellfun(@(x)(strjoin(x(2:end), '/')), a, 'un', 0);

    % val files
    valFn = imdb.images.name(imdb.images.set==2);
    imdb.images.name = vertcat(b, valFn);


    save(fullfile(expDir, 'imdb', 'imdb-cnn.mat'), '-struct', 'imdb');
else
    imdb = load(fullfile(expDir, 'imdb', 'imdb-cnn.mat'));
end




%% Train the network for rectangles

[net, info] = cnn_edge_train(...
                      'baseNetwork', [], ...
                      'expDir', fullfile(expDir, 'cnn-train'), ...
                      'batchNormalization', true, ...
                      'imdbPath', fullfile(expDir, 'imdb', 'imdb-cnn.mat'), ...
                      'networkType', 'dagnn', ...
                      'modelType', 'rectangle-filter', ...
                      'train', struct('gpus', [2]));


% save network
if ~exist(fullfile(expDir, 'cnn_model', 'net_occlusion.mat'), 'file')
    vl_xmkdir(fullfile(expDir, 'cnn_model')); s = saveobj(net);
    save(fullfile(expDir, 'cnn_model', 'net_occlusion.mat'), '-struct', 's');
end




