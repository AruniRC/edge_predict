% Script to create the HOG-response data
%  For each image, 9 HOG edge responses are saved into .mat files.
%  Images are first resized to 224x224
%
% Assumes a list of image filenames is saved in text files, by executing
% the following commands at the console:
%
% $ cd <imgDir>
% $ find train -type f > tmpfile.txt
% $ head -n 100000 tmpfile.txt > files_100k_train.txt
% $ find val -type f > tmpfile.txt
% $ head -n 1000 tmpfile.txt > files_100k_val.txt
%
%   This script will skip the set if 'train' or 'val' folders are already
%   existing under `outDir`.


txtFileDir = '/data/arunirc/datasets/ImageNet/images';
imgDir = '/data/arunirc/datasets/ImageNet/images';
outDir = '/data/arunirc/datasets/ImageNet-hog-8/';
cellSize = 8;
DEBUG = false; % set to `true` for looking at 20 images and their HOGs
stdNorm = true; % weight HOG response by its variance over orientations
visDir = 'data/vis-output';  % output visualizations of histograms

vl_xmkdir(visDir);


%% make val set

if ~exist(fullfile(outDir, 'val'))
    vl_xmkdir(fullfile(outDir, 'val'));
    fid = fopen(fullfile(txtFileDir, 'files_100k_val.txt'));
    valFileNames = textscan(fid,'%s','Delimiter','\n');
    valFileNames = valFileNames{1};
    save_hog_response(imgDir, valFileNames, outDir, cellSize);
else
    disp(sprintf('Val data folder already exists under %s',outDir)); 
end


%% make train set
tic
if ~exist(fullfile(outDir, 'train'))
    vl_xmkdir(fullfile(outDir, 'train'));
    fid = fopen(fullfile(txtFileDir, 'files_100k_train.txt'));
    trainFiletionNames = textscan(fid,'%s','Delimiter','\n');
    trainFileNames = trainFileNames{1};
    save_hog_response(imgDir, trainFileNames, outDir, cellSize);
else
    disp(sprintf('Train data folder already exists under %s',outDir)); 
end
toc


%% Visualize data histograms

imdb = edge_get_imdb(outDir);
val = find(imdb.images.set == 2);
train = find(imdb.images.set == 1);

% sample 1000 images from training set
batch = train(randperm(length(train), 1000));

edgeSet = load_data_subset(imdb, batch);
edgeSetArray = cat(4, edgeSet{:});

if stdNorm
    hogStd = std(edgeSetArray, 0, 3);
    edgeSetArray = bsxfun(@times, edgeSetArray, hogStd);
end
edgeSetVec = edgeSetArray(:);

if DEBUG
    % take a look at the data
    opts.numGpus = 1;
    opts.debug = true;
    inputs = getHogEdgeBatch(opts, imdb, batch(1:20));
end

% default MATLAB histogram on real valued HOG response
figure
histogram(edgeSetVec);

% show percentiles
if stdNorm
    Y = prctile(edgeSetVec, [50 75 95]); % TODO - mnemonic name for Y!
else
    Y = prctile(edgeSetVec, [25 50 75]);
end
hold on
plot(Y, [0 0 0], 'r*')
title('9th HOG histogram')
if stdNorm
    xlim([0 0.02])
else
    xlim([0 0.2])
end

% show the percentiles
disp(Y);

% save figure into visualization output folder
if ~stdNorm
    saveas(gca, fullfile(visDir, 'hist_hog9_2.png')); 
else
    saveas(gca, fullfile(visDir, 'hist_hog9_2_std.png'));
end

% save the percentiles/quantiles
%   -- these values are hard-coded into getHogEdgeBatch()` :P
if ~stdNorm
    hogBinFn = fullfile(visDir, 'hog9_quantized_bins_2.txt');
else
    hogBinFn = fullfile(visDir, 'hog9_quantized_bins_2_std.txt');
end
dlmwrite(hogBinFn,Y);



%% Quantized values for HOG
%   - Quantize the HOG response into 4 bins using pre-computed percentiles
%   - The reponse of 9th HOG is mapped from [0,0.2] to [1,2,3,4]
%   - We can now use a classification loss instead of a regression loss

% take a look at the data
batch = val(1:100);
opts.numGpus = 1;
opts.debug = true;

if stdNorm, opts.stdNorm = true; end

inputs = getHogEdgeBatch(opts, imdb, batch(1:20));

% % bin the histogram according to percentiles
% figure;
% histEdges = [0 Y 0.2];
% histogram(edgeSetVec, histEdges);


