% run_vis_edge_predict


%% 0) Initial settings and paths

setup;
expDir = 'data/edge_alexnet_v1';
imdbPath = fullfile(expDir, 'imdb', 'imdb-cnn.mat')'; 
epoch = 18;  % load network at different epochs
netPath = fullfile(expDir, sprintf('cnn-train/net-epoch-%d.mat', epoch));   % specify which network to use


%% 1) Load network
s = load(netPath);
net = dagnn.DagNN.loadobj(s.net);
net = cnn_imagenet_deploy(net);


%% 2) Evaluate network on subset of dataset

imdb = load(imdbPath);
opts.numGpus = 4;
opts.stdNorm = false;
opts.inputFilterID = [81:95];
opts.outputFilterID = 96;
opts.quantizeOutput = true;
opts.doAbs = true;

rng(0);
% gpuDevice(opts.numGpus);

% grab 100 images from validation set
batch = find(imdb.images.set == 1); % or TRAIN
batch = batch(randperm(length(batch), 20));
inputs = getCNNFilterBatch(opts, imdb, batch);

% set GPU mode ('cpu'/'gpu') and evaluate network
net.mode = 'test';
net.move('gpu');
net.eval(inputs(1:2));


% ground-truth labels
gtLabels = gather(inputs{4});

% network predictions
prob = gather(net.vars(net.getVarIndex('prob')).value);
[~,pred] = max(prob, [], 3);



%% 3) Visualize label histograms

gtLabelVec = gtLabels(:);
gtLabelHist = hist(gtLabelVec, unique(gtLabelVec));

predVec = pred(:);
predLabelHist = hist(predVec, unique(predVec));


figure;
subplot(1,2,1); histogram(gtLabelVec, 'Normalization', 'probability'); 
title('gt label'); xlim([0.5 4.5]); ylim([0 1]);

subplot(1,2,2); histogram(predVec, [0.5:4.5], 'Normalization', 'probability'); 
title('pred label'); xlim([0.5 4.5]); ylim([0 1]);

% save figures
vl_xmkdir(fullfile(expDir, 'cnn-viz'));
saveas(gca, fullfile(expDir, 'cnn-viz', sprintf('label_hist_e%d.png', epoch)));


%% 4) Visualize images and predictions

imageNetDir = '/data/arunirc/datasets/ImageNet/images';
visDir = fullfile(expDir, 'cnn-viz-preds-train');  
vl_xmkdir(visDir);

figure;
fn = imdb.images.name(batch);

for ii = 1:numel(batch)
    
   % load original image
   [a,b,~] = fileparts(fn{ii});
   imFn = [a filesep b '.JPEG'];
   img = imread(fullfile(imageNetDir, imFn));
   img = imresize(img, [224 224]);
   
   subplot(1,3,1); imagesc(img); 
   title('img'); axis('square'); set(gca,'xtick',[]);  set(gca,'ytick',[]);
   
   % show ground-truth labels of missing filter
   subplot(1,3,2); imagesc(gtLabels(:,:,:,ii)); 
   title('gt'); axis('square'); caxis([1 4]);  set(gca,'xtick',[]); set(gca,'ytick',[]);
   
   % show predicted labels of 9th HOG
   subplot(1,3,3); imagesc(pred(:,:,:,ii)); 
   title('pred'); axis('square'); caxis([1 4]);  set(gca,'xtick',[]); set(gca,'ytick',[]); 
   
   pause(2);
   saveas(gca, fullfile(visDir, ['epoch_' num2str(epoch) 'preds_' num2str(ii) '.png']));
    
end

