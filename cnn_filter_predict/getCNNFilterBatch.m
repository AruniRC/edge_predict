% -------------------------------------------------------------------------
function inputs = getCNNFilterBatch(opts, imdb, batch, weights)
% -------------------------------------------------------------------------
% opts.numGpus = 0;
% opts.inputFilterID = [1:10];
% opts.outputFilterID = 12;
% opts.imageNetDir = '/data/arunirc/datasets/ImageNet/images';
% opts.visDir = 'data/vis-output'; 
% opts.debug = false;
% opts.quantizeOutput = true;
% opts.doAbs = true;
t = [-9999 -15.9310   -0.6490   13.2233 9999];

fn = imdb.images.name(batch);

DEBUG = false;

% debug settings
if isfield(opts, 'debug')
    if opts.debug
        figure; 
        DEBUG = true;
        vl_xmkdir(opts.visDir);
        assert(nargin==4);
    end
end

inputMap = cell(1, numel(batch));
outputMap = cell(1, numel(batch));


for ii = 1:numel(batch)
    
   % load data
   dat = load(fullfile(imdb.imageDir, fn{ii}));
   data = dat.x; 
   
   if opts.doAbs
       data = abs(data);
       t = [0   5.5767   14.6122   38.1714   9999];
   end
   
   if DEBUG       
        subplot(1,3,1);
        vl_imarraysc(weights,'spacing',2)
        axis equal ;       
        title('Filter weights');
       
        subplot(1,3,2);
        vl_imarraysc(data,'spacing',2)
        axis equal ;    colormap gray; 
        title('Filter responses');
        
        % get filename of original JPEG image -- for ImageNet only!
        [a,b,~] = fileparts(fn{ii});
        imFn = [a filesep b '.JPEG'];
        img = imread(fullfile(opts.imageNetDir, imFn));
        
        subplot(1,3,3);
        imshow(img);
        axis equal;
        title('Image');
        
        % saveas(gca, fullfile(opts.visDir, [num2str(ii) '.png']));
        
        pause(1);
   end
   
   % select the filter data
   inputData = data(:,:,opts.inputFilterID);
   outputData = data(:,:,opts.outputFilterID);  
   
   if opts.quantizeOutput
       % quantize the output filter response into bins
       % -- pre-compute the percentile bins after dataset is created
       % -- check the last sections of `run_make_data_alexnet.m`       
       outputData = quantizeData(outputData, t);      
   end
   
   inputMap{ii} = inputData;
   outputMap{ii} = outputData;
end

images = single(cat(4, inputMap{:}));
labels = single(cat(4, outputMap{:}));


if opts.numGpus > 0
  images = gpuArray(images) ;
end
inputs = {'input', images, 'label', labels} ;

