% -------------------------------------------------------------------------
function x = get_cnn_filter_response(net, img)
% -------------------------------------------------------------------------

% pre-process image
img = imresize(img, net.meta.normalization.imageSize(1:2));
img = single(img);
img = bsxfun(@minus, img, net.meta.normalization.averageImage);

% evaluate
net.vars(2).precious = true;
net.eval({'input', gpuArray(img)});
x = gather(net.vars(2).value);
