%%

%% Load Kanizsas' Triangle image

imPath = './edge_predict/samples/Kanizsas-Triangle-123i.jpg';
img = imread(imPath);
img = imresize(img, [224 224]); 
% netPath = fullfile(expDir, 'cnn-train/net-epoch-20.mat');
imt = imrotate(img, -90);
figure; imagesc(imt); title('k-t rot -90');
colormap gray

% invert colors (dark lines --> white lines)
imc = imcomplement(imt); 

% dilate image
se = offsetstrel('ball',5,5);
dilatedI = imdilate(imc,se);

% get back original colors
imD = imcomplement(dilatedI);
imD(imD < 200) = 0;  % threshold to have darker lines


%% AlexNet filter output

% load alexnet
clear net;
alexnetPath = 'data/models/imagenet-caffe-alex.mat';
net = load(alexnetPath);
net = dagnn.DagNN.fromSimpleNN(net, 'CanonicalNames', true);

% get all filter responses
net.move('gpu');
net.mode = 'test';
x = get_cnn_filter_response(net, imD);

% all alexnet filters
alexWeights = squeeze(gather(net.params(1).value));

figure;
vl_imarraysc(alexWeights,'spacing',2)
axis equal ;


%% Filter prediction network

% load the prediction network
expDir = 'data/edge_alexnet_v1';
imdbPath = fullfile(expDir, 'imdb', 'imdb-cnn.mat')'; 
epoch = 10;  % load network at different epochs
netPath = fullfile(expDir, sprintf('cnn-train/net-epoch-%d.mat', epoch));   % specify which network to use

s = load(netPath);
net = dagnn.DagNN.loadobj(s.net);
net = cnn_imagenet_deploy(net);


%% Network predictions

% select the filter data
x = abs(x);
inputData = x(:,:,opts.inputFilterID);
outputData = x(:,:,opts.outputFilterID);  

net.mode = 'test';
net.move('gpu');
net.eval({'input', gpuArray(inputData)});

% network predictions
prob = gather(net.vars(net.getVarIndex('prob')).value);
[~,pred] = max(prob, [], 3);

%% visualize

figure;

% show dilated triangle image
vl_tightsubplot(1,3,1);
imagesc(imD); colormap gray;
title('image'); 
axis square ;

% show predicted response
vl_tightsubplot(1,3,2);
imagesc(pred); % colormap default;
title('predicted'); axis square ;


% filter response values
vl_tightsubplot(1,3,3); 
imagesc(outputData); % colormap default;
title('filter values'); axis square ;










