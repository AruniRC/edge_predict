% run_vis_edge_predict


%% 0) Initial settings and paths

setup;
expDir = '/data2/arunirc/Research/edge_predict/data/edge_hog-8-std';
imdbPath = fullfile(expDir, 'imdb', 'imdb-cnn.mat')'; 
epoch = 1;  % load network at different epochs
netPath = fullfile(expDir, sprintf('cnn-train/net-epoch-%d.mat', epoch));   % specify which network to use


%% 0.5) Visualize HOG edges on Kanizsa's triangle image

%{

% Kanizsas' Triangle image
imPath = './edge_predict/samples/Kanizsas-Triangle-123i.jpg';
img = imread(imPath);
img = imresize(img, [224 224]);
netPath = fullfile(expDir, 'cnn-train/net-epoch-20.mat');

% calculate full HOG
cellSize = 4;
feat = vl_hog(im2single(img), cellSize, 'variant', 'dalaltriggs') ;
imhog = vl_hog('render', feat, 'verbose', 'variant', 'dalaltriggs') ;


% get rid of the 4 types of L2 normalizations
%   -- replicates the first kind of L2-normalized HOG for the other 4
feat_1 = feat(:,:,1:9);
feat_1 = repmat(feat_1, [1 1 4]);
imhog_1 = vl_hog('render', feat_1, 'verbose', 'variant', 'dalaltriggs') ;


% get rid of the last edge orientation
feat_2 = feat(:,:,1:8);
feat_2 = cat(3, feat_2, zeros([size(feat_2,1) size(feat_2,2)], class(feat_2)));
feat_2 = repmat(feat_2, [1 1 4]);
imhog_2 = vl_hog('render', feat_2, 'verbose', 'variant', 'dalaltriggs') ;

hog_diff = abs(imhog_1 - imhog_2);

figure; 
vl_tightsubplot(1,5,1); imshow(img); title('original')          % image
vl_tightsubplot(1,5,2); imshow(imhog); title('HOG-36D')        % full hog filters
vl_tightsubplot(1,5,3); imshow(imhog_1); title('HOG-9D')       % single normalized hog
vl_tightsubplot(1,5,4); imshow(imhog_2); title('HOG-8D')      % removed one edge response
vl_tightsubplot(1,5,5); imshow(hog_diff); title('HOG-diff')     % response map difference

%}


%% 1) Load network
s = load(netPath);
net = dagnn.DagNN.loadobj(s.net);
net = cnn_imagenet_deploy(net);


%% 2) Evaluate network on subset of dataset

imdb = load(imdbPath);
opts.numGpus = 1;
opts.stdNorm = 1;

rng(0);

% grab 100 images from validation set
batch = find(imdb.images.set == 1); % or TRAIN
batch = batch(randperm(length(batch), 100));
inputs = getHogEdgeBatch(opts, imdb, batch);

% set GPU mode ('cpu'/'gpu') and evaluate network
net.move('gpu');
net.eval(inputs(1:2));

% ground-truth labels
gtLabels = gather(inputs{4});
gtLabelVec = gtLabels(:);
gtLabelHist = hist(gtLabelVec, unique(gtLabelVec));

% network predictions
prob = gather(net.vars(net.getVarIndex('prob')).value);
[~,pred] = max(prob, [], 3);
predVec = pred(:);
predLabelHist = hist(predVec, unique(predVec));


%% 3) Visualize label histograms

figure;
subplot(1,2,1); histogram(gtLabelVec, 'Normalization', 'probability'); 
title('gt label'); xlim([0.5 4.5]); ylim([0 1]);

subplot(1,2,2); histogram(predVec, [0.5:4.5], 'Normalization', 'probability'); 
title('pred label'); xlim([0.5 4.5]); ylim([0 1]);

% save figures
vl_xmkdir(fullfile(expDir, 'cnn-viz'));
saveas(gca, fullfile(expDir, 'cnn-viz', sprintf('label_hist_e%d.png', epoch)));


%% 4) Visualize images and predictions

imageNetDir = '/data/arunirc/datasets/ImageNet/images';
visDir = fullfile(expDir, 'cnn-viz-preds-train-stdDev');  
vl_xmkdir(visDir);

figure;
fn = imdb.images.name(batch);
for ii = 1:numel(batch)
    
   % load original image
   [a,b,~] = fileparts(fn{ii});
   imFn = [a filesep b '.JPEG'];
   img = imread(fullfile(imageNetDir, imFn));
   img = imresize(img, [224 224]);
   
   subplot(2,3,1); imagesc(img); 
   title('img'); axis('square'); set(gca,'xtick',[]);  set(gca,'ytick',[]);
   
   % show ground-truth labels of 9th HOG
   subplot(2,3,2); imagesc(gtLabels(:,:,:,ii)); 
   title('gt'); axis('square'); caxis([1 4]);  set(gca,'xtick',[]); set(gca,'ytick',[]);
   
   % show predicted labels of 9th HOG
   subplot(2,3,3); imagesc(pred(:,:,:,ii)); 
   title('pred'); axis('square'); caxis([1 4]);  set(gca,'xtick',[]); set(gca,'ytick',[]); 
   
   % show the whole HOG direction visualization
   dat = load(fullfile(imdb.imageDir, fn{ii}));
   data = dat.edgeSet;
   
   % weight by standard deviation over orientations
   %    -- spurious HOG activation patches have high response at all
   %    orientations
   %    -- this down-weights responses at low-contrast patches
   %    -- partially un-do the effect of local contrast normalization
   stdDev = std(data, 0, 3); 
   data = bsxfun(@times, data, stdDev);
   
   feat_1 = repmat(data, [1 1 4]); % repeat to fill up the 4 L2-norm schemes
   imhog_1 = vl_hog('render', feat_1, 'verbose', 'variant', 'dalaltriggs') ;
   subplot(2,3,4); imagesc(imhog_1); 
   title('whole HOG'); axis('square');  set(gca,'xtick',[]); set(gca,'ytick',[]);
   
   % show 9th HOG response map
   hog_9 = data(:,:,9);
   subplot(2,3,5); imagesc(hog_9); 
   title('HOG-9'); axis('square');  set(gca,'xtick',[]); set(gca,'ytick',[]);
   
   
   %{
   % testing . . .
   stdDev = std(data, 0, 3); 
   featNorm = bsxfun(@times, data, stdDev);
   feat_2 = repmat(featNorm, [1 1 4]); % repeat to fill up the 4 L2-norm schemes
   imhog_2 = vl_hog('render', feat_2, 'verbose', 'variant', 'dalaltriggs') ;
   subplot(2,3,6); imagesc(imhog_2); 
   title('HOG std'); axis('square');  set(gca,'xtick',[]); set(gca,'ytick',[]);
   %}
   
   
   % show 9th HOG direction map
   feat_9 = cat(3, zeros([size(hog_9,1) size(hog_9,2) 8], 'single'), hog_9);  
   feat_9_rep = repmat(feat_9, [1 1 4]);
   imhog_9 = vl_hog('render', feat_9_rep, 'verbose', 'variant', 'dalaltriggs') ;
   subplot(2,3,6); imagesc(imhog_9); 
   title('HOG-9-dir'); axis('square');   set(gca,'xtick',[]); set(gca,'ytick',[]);
   
   
   pause(2);
   saveas(gca, fullfile(visDir, ['epoch_' num2str(epoch) 'preds_' num2str(ii) '.png']));
    
end




