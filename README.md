# Self-supervised training by predicting missing edges


## Network pre-training: Predict missing gradient edges

We put in a subset of 10k images from ImageNet at 150x150 resolution. A horizontal and a vertical gradient filter are run on an image, yielding real-valued edgemaps, `Gx` and `Gy`. 
One of the filter outputs is the input to the image, and the other one is the target to be predicted by the network.


### Network architecture

The network has a single decimating first layer that takes the 150x150 single channel image and produces a 75x75 feature map.
The following convolution layers are all padded such that the spatial dimensions of their feature maps are maintained. The spatial size reduction is done only to efficiently train on GPU. 

The network is trained with SGD using the L2 loss and batch normalization.

The full network architecture diagram can be download from [here (PDF)](samples/net-edge-predict-arch.pdf).

### Code settings

The following settings can be used in the script `run_gradient_train.m`

+ v1 - predict horizontal from vertical
+ v2 - predict vertical from horizontal
+ v3 - mixed batches of both gradients


### Sample predictions

Some samples of *input*, *ground truth* and *prediction* images tiled horizontally are shown below. The "v3" network trained on mixed pairs is used for these predictions done on an unseen validation set.

![img1_1](samples/epoch_20preds_1_y.png)  ![img1_2](samples/epoch_20preds_1.png)

![img2_1](samples/epoch_20preds_20_y.png)  ![img2_2](samples/epoch_20preds_20.png)


### Training curve

Plot showing the training and validation L2 objective over 20 epochs of training.

![v3_edge_train_plot](samples/net-train-edge-v3.png)


---

## TODO - Classification tasks

The network has to be modified (made "fine-tune-able") so that it can be used for classification.

Dataset: CUB


## TODO - Boundary detection tasks

Dataset: BSDS

