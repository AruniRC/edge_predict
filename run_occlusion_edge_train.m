%% Run occlusion edge prediction training
%   Given an edgemap (either binary or with gradient magnitudes) as a
%   "canvas" image, we put an occluding shape (a rectangle) the same
%   colour as the background. The network has to predict the outline or
%   border of this "borderless occluder" shape. We can vary the difficulty
%   of the settings by modifying the threshold and sigma values of the
%   Canny edge detector used to get the "canvas" image.


rng(0);
expDir = 'data/edge_occlusion_v1.6.4-wider';


%% Make the IMDb
%   - ImageNet 100k training images (1k validation)
if ~exist(fullfile(expDir, 'imdb', 'imdb-cnn.mat'))
    imagenetDir = '/data/arunirc/datasets/ImageNet/images/';
    imdb = imagenet100k_get_imdb(imagenetDir);
    vl_xmkdir(fullfile(expDir, 'imdb'));
    save(fullfile(expDir, 'imdb', 'imdb-cnn.mat'), '-struct', 'imdb');
else
    imdb = load(fullfile(expDir, 'imdb', 'imdb-cnn.mat'));
end


%% Test the get batch code

% opts.numGpus = 0;
% opts.edgeEnergy = true;
% opts.method = 'partial-border';
% rng(0);
% batch = randperm(numel(imdb.images.name), 10);
% inputs = getOcclusionEdgeBatch(opts, imdb, batch);
% 
% images = inputs{2};
% labels = inputs{4};
% classwt = inputs{6};
% figure;
% for ii = 1:numel(batch)
%    im = gather(images(:,:,:,ii));
%    gt = gather(labels(:,:,:,ii));
%    wt = gather(classwt(:,:,:,ii));
%    vl_tightsubplot(1,3,1); imagesc(im); axis('image');
%    vl_tightsubplot(1,3,2); imagesc(gt); axis('image');
%    vl_tightsubplot(1,3,3); imagesc(wt); axis('image');
%    pause;
% end



%% Train the network

[net, info] = cnn_edge_train(...
                      'baseNetwork', [], ...
                      'expDir', fullfile(expDir, 'cnn-train'), ...
                      'batchNormalization', true, ...
                      'imdbPath', fullfile(expDir, 'imdb', 'imdb-cnn.mat'), ...
                      'networkType', 'dagnn', ...
                      'modelType', 'edge-occlusion-wider', ...
                      'occlusionType', 'partial-border', ...
                      'quantizeInputs', true, ...    % use real-valued inputs
                      'train', struct('gpus', [2]));


% save network
if ~exist(fullfile(expDir, 'cnn_model', 'net_occlusion.mat'), 'file')
    vl_xmkdir(fullfile(expDir, 'cnn_model')); s = saveobj(net);
    save(fullfile(expDir, 'cnn_model', 'net_occlusion.mat'), '-struct', 's');
end







