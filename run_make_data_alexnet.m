% Script to create the HOG-response data
%  For each image, 9 HOG edge responses are saved into .mat files.
%  Images are first resized to 224x224
%
% Assumes a list of image filenames is saved in text files, by executing
% the following commands at the console:
%
% $ cd <imgDir>
% $ find train -type f > tmpfile.txt
% $ head -n 100000 tmpfile.txt > files_100k_train.txt
% $ find val -type f > tmpfile.txt
% $ head -n 1000 tmpfile.txt > files_100k_val.txt
%
%   This script will skip the set if 'train' or 'val' folders are already
%   existing under `outDir`.


txtFileDir = '/data/arunirc/datasets/ImageNet/images';
imgDir = '/data/arunirc/datasets/ImageNet/images';
outDir = '/data/arunirc/datasets/ImageNet-alexnet-filter/';
netPath = 'data/models/imagenet-caffe-alex.mat';
DEBUG = false; % set to `true` for looking at 20 images and their HOGs
visDir = 'data/alexnet/vis-output';  % output visualizations of histograms

vl_xmkdir(visDir);

%% Load network

net = load(netPath);
net = dagnn.DagNN.fromSimpleNN(net, 'CanonicalNames', true);

%% Display filters

weights = squeeze(gather(net.params(1).value));

figure;
vl_imarraysc(weights,'spacing',2)
axis equal ;


%% make val set

if ~exist(fullfile(outDir, 'val'))
    vl_xmkdir(fullfile(outDir, 'val'));
    fid = fopen(fullfile(txtFileDir, 'files_100k_val.txt'));
    valFileNames = textscan(fid,'%s','Delimiter','\n');
    valFileNames = valFileNames{1};
    save_alexnet_response(net, imgDir, valFileNames, outDir);
else
    disp(sprintf('Val data folder already exists under %s',outDir)); 
end


%% make train set
tic
if ~exist(fullfile(outDir, 'train'))
    vl_xmkdir(fullfile(outDir, 'train'));
    fid = fopen(fullfile(txtFileDir, 'files_100k_train.txt'));
    trainFileNames = textscan(fid,'%s','Delimiter','\n');
    trainFileNames = trainFileNames{1};
    save_alexnet_response(net, imgDir, trainFileNames, outDir);
else
    disp(sprintf('Train data folder already exists under %s',outDir)); 
end
toc

%% Visualize the batches

imdb = edge_get_imdb(outDir);
val = find(imdb.images.set == 2);
train = find(imdb.images.set == 1);

imdb.imageDir = '/data/arunirc/datasets';

%%
% sample images from training set
rng(0);
batch = train(randperm(length(train), 1000));

opts.numGpus = 0;
opts.visDir = 'data/vis-output'; 
opts.debug = true;
opts.quantizeOutput = false;
opts.doAbs = true;

inputs = getRectangleEdgeBatch(opts, imdb, batch);
inputResponse = inputs{2};
outputResponse = inputs{4};

resVec = outputResponse(:);

% show the histogram of target response values
figure; histogram(resVec);  
title('Hist(response)');
hold on;

% show the quantiles
percBins = prctile(resVec, [25 50 75]);
plot(percBins, [0 0 0], 'r*')

disp(percBins)

