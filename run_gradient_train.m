%% Run edge prediction training
%   Given the horizontal derivative filter response
%   predict the vertical filter's responses.


rng(0);
expDir = 'data/edge_gradient_v3';


%% Make the IMDb
%   - ImageNet 100k training images (1k validation)
if ~exist(fullfile(expDir, 'imdb', 'imdb-cnn.mat'))
    imagenetDir = '/data/arunirc/datasets/ImageNet/images/';
    imdb = imagenet100k_get_imdb(imagenetDir);
    vl_xmkdir(fullfile(expDir, 'imdb'));
    save(fullfile(expDir, 'imdb', 'imdb-cnn.mat'), '-struct', 'imdb');
else
    imdb = load(fullfile(expDir, 'imdb', 'imdb-cnn.mat'));
end


%% Test the get batch code

%{
opts.doAbs = true ;
opts.quantizeOutput = false ;
opts.quantizeInputs = false;
opts.numGpus = 0;
rng(0);
batch = randperm(numel(imdb.images.name), 10);
inputs = getRectangleEdgeBatch(opts, imdb, batch);

images = inputs{2};
labels = inputs{4};

figure;

for ii = 1:numel(batch)
   im = gather(images(:,:,:,ii));
   gt = gather(labels(:,:,:,ii));
   vl_tightsubplot(1,3,1); imagesc(im); axis('image');
   vl_tightsubplot(1,3,2); imagesc(gt); axis('image');
   pause;
end
%}

%% Plot histogram values
%{
% sample images from training set
rng(0);
train = find(imdb.images.set==1);
batch = train(randperm(length(train), 1000));

opts.numGpus = 0;
opts.visDir = fullfile(expDir, 'viz'); 
opts.debug = false;
opts.quantizeOutput = false;
opts.doAbs = true;

inputs = getRectangleEdgeBatch(opts, imdb, batch);
inputResponse = inputs{2};
outputResponse = inputs{4};

resVec = outputResponse(:);
resVec(resVec>0.2) = 0.2;

% show the histogram of target response values
figure; histogram(resVec);
title('Hist(response)');
hold on;

% show the quantiles
percBins = prctile(resVec, [25 50 75]);
plot(percBins, [0 0 0], 'r*')

% print quantiles to console
disp(percBins);   % 0.031373,0.10588,0.30588

dlmwrite(fullfile(expDir, 'output_perc_bins.txt'), percBins);

%% visualize quantized output response
opts.numGpus = 0;
opts.visDir = fullfile(expDir, 'viz'); 
opts.debug = true;
opts.quantizeOutput = false;
opts.doAbs = true;
train = find(imdb.images.set==1);
batch = train(randperm(length(train), 20));
inputs = getRectangleEdgeBatch(opts, imdb, batch);
%}


%% Train the network

[net, info] = cnn_edge_train(...
                      'baseNetwork', [], ...
                      'expDir', fullfile(expDir, 'cnn-train'), ...
                      'batchNormalization', true, ...
                      'imdbPath', fullfile(expDir, 'imdb', 'imdb-cnn.mat'), ...
                      'networkType', 'dagnn', ...
                      'modelType', 'gradient-filter', ...
                      'quantizeInputs', false, ...    % use real-valued inputs
                      'quantizeOutput', false, ...
                      'train', struct('gpus', []));


% save network
if ~exist(fullfile(expDir, 'cnn_model', 'net_edge.mat'), 'file')
    vl_xmkdir(fullfile(expDir, 'cnn_model')); s = saveobj(net);
    save(fullfile(expDir, 'cnn_model', 'net_occlusion.mat'), '-struct', 's');
end







