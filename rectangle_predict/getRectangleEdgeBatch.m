% -------------------------------------------------------------------------
function inputs = getRectangleEdgeBatch(opts, imdb, batch)
% -------------------------------------------------------------------------
% opts.numGpus = 0;
% opts.imageNetDir = '/data/arunirc/datasets/ImageNet/images';
% opts.visDir = 'data/vis-output'; 
% opts.debug = false;
% opts.quantizeOutput = true;
% opts.quantizeInput = false;
% opts.doAbs = true;
opts.edgeInput = 'mixed';
imSize = 150;
t = [-9999, 0.0314,   0.1059,    0.1999, 9999]; % pre-computed

fn = imdb.images.name(batch);

DEBUG = false;

% debug settings
if isfield(opts, 'debug')
    if opts.debug
        figure; 
        DEBUG = true;
        vl_xmkdir(opts.visDir);
    end
end

inputMap = cell(1, numel(batch));
outputMap = cell(1, numel(batch));


for ii = 1:numel(batch)
    
   % load image
   try
        im = imread(fullfile(imdb.imageDir, fn{ii}));       
   catch ME
       disp(ME);
       continue;
   end
   
   im = imresize(im, [imSize imSize]);
   
   
   % directional gradients 
   % - for each channel, then max
   im = im2single(im);
   
   if numel(size(im)) ~= 3
      im = cat(3, {im, im, im}); 
      im = cat(3, im{:});
   end
   
   
   [Gx1,Gy1] = imgradientxy(im(:,:,1));
   [Gx2,Gy2] = imgradientxy(im(:,:,2));
   [Gx3,Gy3] = imgradientxy(im(:,:,3));
   
   Gx = cat(3, Gx1, Gx2, Gx3);  Gx = max(Gx, [], 3);
   Gy = cat(3, Gy1, Gy2, Gy3);  Gy = max(Gy, [], 3); 
   
   if opts.doAbs
       Gx = abs(Gx);
       Gy = abs(Gy);
   end

   % select the filter data
   switch opts.edgeInput
      case 'y' 
        % v1
        inputData = Gy;
        outputData = Gx;  
      case 'x'
        % v2
        inputData = Gx;
        outputData = Gy; 
      case 'mixed'
        % v3
        if rand > 0.5
          inputData = Gx;
          outputData = Gy;  
        else
          inputData = Gy;
          outputData = Gx; 
        end
      otherwise
         error('Incorrect edge option.'); 
    end
   
   outputData = imresize(outputData, [floor(imSize/2) floor(imSize/2)], 'bilinear');
   outputMap{ii} = outputData;

   inputMap{ii} = inputData;
   
   
   if opts.quantizeOutput
       % quantize the output filter response into bins    
       % outputQuantized = -1*ones(size(outputData), 'single');
       % outputQuantized(outputData > 0.2) = 1;
       outputData(outputData>0.2) = 0.2;
       outputQuantized = quantizeData(outputData, t);
       outputQuantized = imresize(outputQuantized, [floor(imSize/2) floor(imSize/2)], 'nearest');
       outputMap{ii} = single(outputQuantized);
   end
   
   
   if isfield(opts, 'quantizeInput')
       if opts.quantizeInput
           inputQuantized = -1*ones(size(inputData), 'single');
           inputQuantized(inputData > 0.2) = 1;
           % inputQuantized = imresize(inputQuantized, [floor(imSize/2) floor(imSize/2)], 'nearest');
           inputMap{ii} = single(inputQuantized);   
       end
   end
   
   
   if DEBUG     
      subplot(2,2,1);  imagesc(outputData); axis square;         title('out'); 
      if exist('outputQuantized', 'var')
        subplot(2,2,2);  imagesc(outputQuantized); axis square;    title('out-qt');     
      end
      subplot(2,2,3);  imagesc(inputData); axis square;          title('input');  
      subplot(2,2,4);  imshow(im); axis square;                  
      % drawnow;
      pause(2);       
   end 
   
end

images = single(cat(4, inputMap{:}));
labels = single(cat(4, outputMap{:}));

if opts.numGpus > 0
  images = gpuArray(images) ;
end
inputs = {'input', images, 'label', labels} ;

