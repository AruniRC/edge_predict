function net = getNetworkFinetunable(modelPath, varargin)
%getNetworkFinetunable Modifies a edge-prediction network for general
%fine-tuning for downstream tasks
%	The network from pre-training on missing edge prediction is modified
%	here for general-purpose classification. The modifications are:
%
%	1. add a new input layer for images: HxWx3 ---> hxwx16. 
%   2. repmat the second layer filters to be compatible with new input 
%       layer.
%   3. add a global average pooling (GAP) layer at the end.
% 
%
% Input:
% -----
%	modelPath 	- 	Path to where the DAG model is saved.
%
%
% Input name/value pairs:
% ----------------------
%   numInputImageChannels - The number of channels in the input image. 
%                           Default: 3.
%   
%   numInputImageSize     - Height and width of input image.
%                           Default: [150 150];
%
%   numOutputChannels     - The first CNN layer takes in the image and maps
%                           it to `numOutputChannel` channels.
%                           Default: 16.
%
%   numClass              - Number of categories. This determines the
%                           output size of the 'prediction' layer of the
%                           network, before the softmax loss layer.
%                           Default: 200 (for the CUB-200 dataset).
%   
%   taskType              - The type of task, 'classification',
%                           'regression' or 'segmentation'.
%                           Default: 'classification'.
%


% 	image layer: HxWx3 --> hxwx16	
opts.numInputImageChannels = 3;
opts.numInputImageSize = [150 150];
opts.numOutputChannels = 16;
%   classification layer
opts.numClass = 200;
opts.taskType = 'classification';
%   misc. options
opts.debug = true;
opts = vl_argparse(opts, varargin);



% -------------------------------------------------------------------------
% preprocess the original network

% load the DAG model
% modelPath = fullfile(expDir, 'cnn_model', 'net_occlusion.mat');
net = dagnn.DagNN.loadobj(modelPath); 

% print(net, {'input', [150 150 3]}, 'Format', 'dot')

% fold in BatchNorm layers
dagMergeBatchNorm(net);
dagRemoveLayersOfType(net, 'dagnn.BatchNorm') ;

% remove old loss layer
dagRemoveLayersOfType(net, 'dagnn.Loss') ;

% remove the regression layer 
% -- maps 512 channels to a single channel at each spatial location
% -- in a linear structure, usually the last layer after "loss" is removed
layerOrder = net.getLayerExecutionOrder();  % order of execution of layers
net.removeLayer(net.layers(layerOrder(end)).name) ;

% modify the original input layer to be compatible with new first layer
net.params(1).value = repmat(net.params(1).value, ...
							 [1 1 opts.numOutputChannels 1]);



% -----------------------------------------------------------------------------
% create new input layer CONV0_1:
%	conv: HxWx3 --> hxwx16
%	Takes in RGB image and has 16-channel output
in_conv = dagnn.Conv('size',[3 3 3 opts.numOutputChannels], ...
						'pad', 1, 'stride', 1, 'hasBias', true);

net.addLayer('conv0_1', in_conv, {'input'}, {'x0'}, {'in_conv_f', 'in_conv_b'});

% weights (filters)
% -- init filters using 'xavier improved'
h = 3 ;
w = 3;
in = 3 ;
out = 16 ;
sc = sqrt(2/(h*w*out)) ;
filters = randn(h, w, in, out, 'single')*sc ;
net.params(net.getParamIndex('in_conv_f')).value = filters;
net.params(net.getParamIndex('in_conv_f')).learningRate = 10;

% bias
net.params(net.getParamIndex('in_conv_b')).value = ... 
							 zeros([opts.numOutputChannels 1], 'single');
net.params(net.getParamIndex('in_conv_b')).learningRate = 10; 
net.params(net.getParamIndex('in_conv_b')).weightDecay = 0; 


% -----------------------------------------------------------------------------
% Layer surgery: adjust inputs and outputs for the new layers
% TODO
net.setLayerInputs('conv1_1', {'x0'});

 

% -----------------------------------------------------------------------------
% add a GAP layer at the current end of network 
%   -- 512 channels at each spatial location of feature map
%	-- sum-pooling layer of the same spatial dimensions as the feature map
layerOrder = net.getLayerExecutionOrder();  % get current order of execution of layers
lastLayerOutputname = net.layers(layerOrder(end)).outputs;
net.addLayer('gap_1', GlobalPooling(), lastLayerOutputname, {'xgap'}, {});

if opts.debug
    % Quick test: does GAP layer sum over the spatial dimensions?
    img = randn([150 150 3], 'single');
    net.eval({'input', img});
    assert(isequal(size(net.vars(end).value), [1 1 512]), ...
                    'Error in Global Average Pooling (GAP) layer.');
end


% -----------------------------------------------------------------------------
% add a layer for classifier weights

% find the feature size of the current final layer output
layerOrder = net.getLayerExecutionOrder();  % get current order of execution of layers
for ii = numel(layerOrder):-1:1
    if ~isempty(net.layers(layerOrder(ii)).params)
        paramName = net.layers(layerOrder(ii)).params{1};
        featureSize = size(net.params(net.getParamIndex(paramName)).value);
        break;
    end
end
if opts.debug
    assert(isequal(featureSize(4), 512), ... 
        'Error in computing the number of channels after GAP layer.');
end

% randomly re-initialise the parameters with 'xavier improved'
h = 1 ;
w = 1;
in = featureSize(4) ;
out = opts.numClass ;
sc = sqrt(2/(h*w*out)) ;
filters = randn(h, w, in, out, 'single')*sc ;
biases = zeros(out, 1, 'single') ;
c_conv = dagnn.Conv('size',[1 1 in out], 'pad', 0, 'stride', 1, 'hasBias', true);

net.addLayer('classifier', c_conv, {'xgap'}, {'prediction'}, ...
                                   {'c_conv_f', 'c_conv_b'});

% weights
net.params(net.getParamIndex('c_conv_f')).value = filters;
net.params(net.getParamIndex('c_conv_f')).learningRate = 10;

% bias
net.params(net.getParamIndex('c_conv_b')).value = biases;
net.params(net.getParamIndex('c_conv_b')).learningRate = 10; 
net.params(net.getParamIndex('c_conv_b')).weightDecay = 0;


% -----------------------------------------------------------------------------
% add a loss layer
net.addLayer('softmaxloss', dagnn.Loss(), {'prediction', 'label'}, {'objective'});

% add layers to show classification performance
net.addLayer('top1err', dagnn.Loss('loss', 'classerror'), ...
                 {'prediction','label'}, 'top1err') ;              
if opts.numClass > 5             
    net.addLayer('top5err', dagnn.Loss('loss', 'topkerror','opts', {'topK',2}), ...
                 {'prediction','label'}, 'top5err') ;
end




