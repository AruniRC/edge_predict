% -------------------------------------------------------------------------
function im = get_shape_image(imW, imH)
% -------------------------------------------------------------------------

%  TODO

% Generate random shape with a 50% chance of border


im = zeros(imH,imW, 3);

for ii = 1:100
   left=floor(rand*imW)+1;
   top=floor(rand*imH)+1; 
   width = floor(rand*imW/2);
   height = floor(rand*imH/2);
    
   im = insertShape(im, 'FilledCircle', [left, top, width, height]);
    
end

