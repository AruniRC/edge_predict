% -------------------------------------------------------------------------
function [im, borderIm] = get_rect_image(imW, imH, varargin)
% -------------------------------------------------------------------------

% TODO: with border!
opts.rotate = false;
opts.border = true;
opts.display = true;
[opts, varargin] = vl_argparse(opts, varargin) ;


im = rand(imH,imW, 3);
borderIm = rand(imH,imW, 1);

for i=1:1000
  left=floor(rand*imW)+1;
  top=floor(rand*imH)+1;
  
  width = floor(rand*imW/2);
  height = floor(rand*imH/2);

  right = left + width;
  bot = top + height;
  
  if right > imW, right = imW; end
  if bot > imH, bot = imH; end

  im(left:right,top:bot, 1)=floor(rand*256);
  im(left:right,top:bot, 2)=floor(rand*256);
  im(left:right,top:bot, 3)=floor(rand*256);
  
  if opts.border
     im(left:right,top, 1)=floor(0); im(left:right,top, 2)=floor(0); im(left:right,top, 3)=floor(0);   
     im(left:right,bot, 1)=floor(0); im(left:right,bot, 2)=floor(0); im(left:right,bot, 3)=floor(0);
     im(left,top:bot, 1)=floor(0); im(left,top:bot, 2)=floor(0); im(left,top:bot, 3)=floor(0);
     im(right,top:bot, 1)=floor(0); im(right,top:bot, 2)=floor(0); im(right,top:bot, 3)=floor(0);
  end
  
  if opts.display
      imagesc(uint8(im));
      drawnow;
  end
  

end

im = uint8(im);