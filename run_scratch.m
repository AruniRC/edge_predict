VGG_FACE_ROOT = '/data2/arunirc/datasets/LFW/lfw_funneled/';
imPath = fullfile(VGG_FACE_ROOT, 'Condoleezza_Rice/Condoleezza_Rice_0002.jpg');


%%

cellSize = 4;
hog = vl_hog(im2single(img), cellSize, 'variant', 'dalaltriggs') ;

% imhog = vl_hog('render', hog, 'verbose', 'variant', 'dalaltriggs') ;

%%

figure;
k = 1;
for i = 1:4
    for j = 1:9
       subplot(4,9,k); imagesc(hog(:,:,k)); colormap(gray);
       k = k+1;
    end
end


%%

figure;
k = 1;
for i = 1:9
    for j = 1:4
       subplot(9, 4, k); imagesc(hog(:,:,k)); 
       k = k+1;
    end
end

%%
figure;
k = 1;
for i = 1:4
    for j = 1:8
       subplot(4, 8, k); imagesc(feat(:,:,k)); 
       k = k+1;
    end
end



