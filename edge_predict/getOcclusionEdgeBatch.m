% -------------------------------------------------------------------------
function inputs = getOcclusionEdgeBatch(opts, imdb, batch)
% -------------------------------------------------------------------------
% opts.numGpus = 0;
% opts.edgeEnergy = false;
% opts.method = 'canny';
% opts.imageSize = 100;

if ~isfield(opts, 'method')
    opts.method = 'canny';
end

imSize = opts.imageSize;

cannyThresh = 0.005;
cannySigma = 2;
edgeWeight = 10; % multiplier for the "edge" class (10x than background)
borderDelThresh = 0.6;

fn = imdb.images.name(batch);

inputMap = cell(1, numel(batch));
outputMap = cell(1, numel(batch));
classWeightMap = cell(1, numel(batch));

for ii = 1:numel(batch)
    
    
    % ---------------------------------------------------------------------
    switch opts.method
        
        case 'canny'
            
            try
                im = imread(fullfile(imdb.imageDir, fn{ii}));    
            catch ME
                ME
                continue;
            end

            % pre-process image
            im = imresize(im, [imSize imSize]);
            if size(im,3) > 1
                im = rgb2gray(im);
            end 
    
            % Canny edge detection on grayscale input image
            edgeIm = edge(im, 'Canny', cannyThresh, cannySigma); % binary edge map

            % multiply binary edge map with edge energy
            if opts.edgeEnergy   	
                [Gmag,~] = imgradient(im, 'sobel');      % sobel gradients   
                Gmag = Gmag ./ (max(Gmag(:)) + eps) ; 
                edgeIm = Gmag .* edgeIm ;
            end 
            
            % overlay occluding rectangle on edge map
            [img, borderIm, info] = get_borderless_rect(edgeIm);
            inputMap{ii} = img; 

            % mark the borders on half-size ground-truth image
            outputImg = get_label_image(imSize, info);
        % -----------------------------------------------------------------    
            
        
        case 'canny-partial-border'
            
            try
                im = imread(fullfile(imdb.imageDir, fn{ii}));    
            catch ME
                ME
                continue;
            end

            % pre-process image
            im = imresize(im, [imSize imSize]);
            if size(im,3) > 1
                im = rgb2gray(im);
            end 
            
            
            
            % Canny edge detection on grayscale input image
            edgeIm = edge(im, 'Canny', cannyThresh, cannySigma); % binary edge map
            
            % Cantor-like border removal
            [img, borderIm, outputImg, info] = get_cantor_border_rect(edgeIm); 
            img = img | (borderIm>0);
          
            % (optional) multiply binary edge map with edge energy
            if opts.edgeEnergy   	
                [Gmag,~] = imgradient(im, 'sobel');      % sobel gradients   
                Gmag = Gmag ./ (max(Gmag(:)) + eps) ; 
                img = Gmag .* img ;
                
                % fit a normal distribution to the edge energy values
                x = img(img>0);
                pd = fitdist(x(:),'Normal');
                t = truncate(pd,0,inf);   % truncate dist to remove negatives
                
                % fill in occluder partial boundaries with gradient values
                % sampled from `t`
                img = img + ( (borderIm>0).*random(t, size(borderIm)) ) ;
                     
                % img = img + ( (borderImClosed>0).*rand(size(borderImClosed)) ) ;
            end 
            
            inputMap{ii} = img; 
            
            % output image is HALF the spatial dimensions of input
            % outputImg = (borderIm==1) .* (~(borderImClosed>0));
            % outputImg = 2*outputImg - 1;
            % outputImg = vl_nnpool(single(outputImg), [2 2], 'stride', 2); % max-pooling to down-sample
            
        % -----------------------------------------------------------------
        
        case 'partial-border'
            % partial border for rectangles on a blank background
            edgeIm = zeros([imSize imSize], 'single');
            [img, borderIm, outputImg, info] = get_cantor_border_rect(edgeIm); 
            img = img | (borderIm>0);
            inputMap{ii} = img; 
        
        case 'gpb'
            error('Method not yet implemented.'); 
        otherwise
            error('Method does not exist.');     
    end
    % ---------------------------------------------------------------------  
    
       
    % instance weights
    instanceWeights = ones(size(outputImg)) + edgeWeight*(outputImg == 1) ;   
    
    outputMap{ii} = outputImg;
    classWeightMap{ii} = instanceWeights;
end

images = single(cat(4, inputMap{:}));
labels = single(cat(4, outputMap{:}));
classWeight = single(cat(4, classWeightMap{:}));

if opts.numGpus > 0
  images = gpuArray(images) ;
  classWeight = gpuArray(classWeight) ;
end
% inputs = {'input', images, 'label', labels, 'classWeight', classWeight} ;
inputs = {'input', images, 'label', labels} ;

% -------------------------------------------------------------------------



% -------------------------------------------------------------------------
function outputImg = get_label_image(imSize, info)
% -------------------------------------------------------------------------
outputImg = -1 * ones([floor(imSize/2) floor(imSize/2)]);
left = floor((info.left)/2);
right = floor((info.right)/2);
top = floor((info.top)/2);
bot = floor((info.bot)/2);  

if top < 1, top = 1; end 
if bot < 1, bot = 1; end
if left < 1, left = 1; end
if right < 1, right = 1; end

outputImg(left:right, top) = 1; 
outputImg(left:right, bot) = 1; 
outputImg(left, top:bot) = 1; 
outputImg(right, top:bot) = 1;  



% -------------------------------------------------------------------------
function [im, borderIm, info] = get_borderless_rect(im)
% -------------------------------------------------------------------------
imH = size(im,1);
imW = size(im,2);
borderIm = -1*ones([imH,imW]);

% draw borderless rectangle
left=floor(rand*imW/2)+1;
top=floor(rand*imH/2)+1;
% rectangle should be at least 10% of whole image
width = floor(rand*imW/2 + sqrt(imW)) ;
height = floor(rand*imH/2 + sqrt(imH));

% check for image dimensions
right = left + width;
bot = top + height;
if right > imW, right = imW; end
if bot > imH, bot = imH; end

% draw the occluding rectangle without borders on the input image
im(left:right,top:bot, 1) = 0;
 
% mark the borders on a separate image
borderIm(left:right, top) = 1; 
borderIm(left:right, bot) = 1; 
borderIm(left, top:bot) = 1; 
borderIm(right, top:bot) = 1; 

info.left = left;
info.right = right;
info.top = top;
info.bot = bot;


% -------------------------------------------------------------------------
function [im, borderIm, borderGT, info] = get_cantor_border_rect(im)
% -------------------------------------------------------------------------
imH = size(im,1);
imW = size(im,2);
borderIm = -1*ones([imH,imW]);

% draw borderless rectangle
left=floor(rand*imW/2)+1;
top=floor(rand*imH/2)+1;
width = floor(rand*imW/2 + sqrt(imW)) ;
height = floor(rand*imH/2 + sqrt(imH));

% check for image dimensions
right = left + width;
bot = top + height;
if right > imW, right = imW; end
if bot > imH, bot = imH; end

% draw the occluding rectangle without borders on the input image
im(left:right,top:bot, 1) = 0;
 
% mark the borders on a separate image
borderIm(left:right, top) = 1;   
borderIm(left:right, bot) = 1; 
borderIm(left, top:bot) = 1; 
borderIm(right, top:bot) = 1; 

% delete middle thirds -- like the beginning of a Cantor set
hw = floor(width/3);
hh = floor(height/3);
borderIm(left+hw:right-hw, top) = -1;   
borderIm(left+hw:right-hw, bot) = -1; 
borderIm(left, top+hh:bot-hh) = -1; 
borderIm(right, top+hh:bot-hh) = -1; 

info.left = left;
info.right = right;
info.top = top;
info.bot = bot;
info.hw = hw;
info.hh = hh;

borderGT = get_cantor_label_image(imH, info);


% -------------------------------------------------------------------------
function outputImg = get_cantor_label_image(imSize, info)
% -------------------------------------------------------------------------
outputImg = -1 * ones([floor(imSize/2) floor(imSize/2)]);
left = floor((info.left)/2);
right = floor((info.right)/2);
top = floor((info.top)/2);
bot = floor((info.bot)/2);  
hw = floor((info.hw)/2);
hh = floor((info.hh)/2);

if top < 1, top = 1; end 
if bot < 1, bot = 1; end
if left < 1, left = 1; end
if right < 1, right = 1; end
if hw < 1, hw = 1; end
if hh < 1, hh = 1; end


outputImg(left+hw:right-hw, top) = 1;   
outputImg(left+hw:right-hw, bot) = 1; 
outputImg(left, top+hh:bot-hh) = 1; 
outputImg(right, top+hh:bot-hh) = 1; 