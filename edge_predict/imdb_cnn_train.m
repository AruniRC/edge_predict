function imdb_cnn_train(imdb, opts, varargin)
% Train a CNN model on a dataset supplied by imdb

opts.lite = false ;
opts.numFetchThreads = 10 ;
opts.train.batchSize = opts.batchSize ;
opts.train.numEpochs = opts.numEpochs ;
opts.train.continue = true ;
opts.train.gpus = opts.useGpu ;
opts.train.prefetch = false ;
opts.train.learningRate = opts.learningRate;
opts.train.expDir = opts.expDir ;
opts = vl_argparse(opts, varargin) ;

% -------------------------------------------------------------------------
%                                                    Network initialization
% -------------------------------------------------------------------------

net = initializeCNNNetwork(imdb, opts) ;

% Initialize average image
if isempty(net.meta.normalization.averageImage) || opts.rgbJitter
    
    imageStatsPath = fullfile(opts.expDir, 'imageStats.mat') ;
    if exist(imageStatsPath)
        load(imageStatsPath, 'averageImage', 'rgbMean', 'rgbCovariance') ;
    else
        meta.normalization.averageImage = [];
        meta.normalization.keepAspect = net.meta.normalization.keepAspect;
        meta.normalization.border = net.meta.normalization.border;
        meta.normalization.imageSize = net.meta.normalization.imageSize;
        meta.normalization.interpolation = net.meta.normalization.interpolation;
        meta.augmentation.rgbVaraince = [];
        meta.augmentation.transformation = 'none';
        [averageImage, rgbMean, rgbCovariance] = getImageStats(imdb, meta, opts) ;
        save(imageStatsPath, 'averageImage', 'rgbMean', 'rgbCovariance') ;
    end
    
    if isempty(net.meta.normalization.averageImage)
        net.meta.normalization.averageImage = averageImage ;
    end
    if opts.rgbJitter
        net.meta.augmentation.rgbVariance = rgbCovariance ;
    end
end

% -------------------------------------------------------------------------
%                                               Stochastic gradient descent
% -------------------------------------------------------------------------

isDag = isa(net, 'dagnn.DagNN');
fn = getBatchWrapper(net.meta, opts, isDag);

if isDag
    [net,info] = cnn_train_dag(net, imdb, fn, opts.train) ;
else
    [net,info] = cnn_train(net, imdb, fn, opts.train, 'conserveMemory', true) ;
end

net = net_deploy(net) ;
% Save model
save(fullfile(opts.expDir, 'final-model.mat'), 'net', 'info', '-v7.3');



function [averageImage, rgbMean, rgbCovariance] = getImageStats(imdb, meta, opts)
% -------------------------------------------------------------------------
train = find(imdb.images.set == 1) ;
train = train(1: 1: end);
bs = 256 ;
fn = getBatchWrapper(meta, opts, false) ;
for t=1:bs:numel(train)
  batch_time = tic ;
  batch = train(t:min(t+bs-1, numel(train))) ;
  fprintf('collecting image stats: batch starting with image %d ...', batch(1)) ;
  temp = fn(imdb, batch) ;
  temp = temp{1};
%   temp = cat(4, temp{1}{:});
  z = reshape(permute(temp,[3 1 2 4]),3,[]) ;
  n = size(z,2) ;
  avg{t} = mean(temp, 4) ;
  rgbm1{t} = sum(z,2)/n ;
  rgbm2{t} = z*z'/n ;
  batch_time = toc(batch_time) ;
  fprintf(' %.2f s (%.1f images/s)\n', batch_time, numel(batch)/ batch_time) ;
end
averageImage = mean(cat(4,avg{:}),4) ;
rgbm1 = mean(cat(2,rgbm1{:}),2) ;
rgbm2 = mean(cat(3,rgbm2{:}),3) ;
rgbMean = rgbm1 ;
rgbCovariance = rgbm2 - rgbm1*rgbm1' ;



% -------------------------------------------------------------------------
function saveNetwork(fileName, net)
% -------------------------------------------------------------------------
layers = net.layers;

% Replace the last layer with softmax
layers{end}.type = 'softmax';
layers{end}.name = 'prob';

% Remove fields corresponding to training parameters
ignoreFields = {'filtersMomentum', ...
                'biasesMomentum',...
                'filtersLearningRate',...
                'biasesLearningRate',...
                'filtersWeightDecay',...
                'biasesWeightDecay',...
                'class'};
for i = 1:length(layers),
    layers{i} = rmfield(layers{i}, ignoreFields(isfield(layers{i}, ignoreFields)));
end
classes = net.meta.classes;
meta = net.meta;
save(fileName, 'layers', 'classes', 'meta');


% -------------------------------------------------------------------------
function fn = getBatchWrapper(meta, opts, isDag)
% -------------------------------------------------------------------------
useGpu = numel(opts.useGpu) > 0;

bopts.numThreads = opts.numFetchThreads;
bopts.imageSize = meta.normalization.imageSize ;
bopts.border = meta.normalization.border ;
bopts.averageImage = meta.normalization.averageImage ;
bopts.rgbVariance = meta.augmentation.rgbVariance ;
bopts.transformation = meta.augmentation.transformation ;


if isDag
    fn = @(imdb,batch) getBatchDagNN(imdb, batch, bopts, useGpu) ;
else
    fn = @(imdb,batch) getBatch(imdb, batch, bopts) ;
end

% -------------------------------------------------------------------------
function [im,labels] = getBatch(imdb, batch, opts)
% -------------------------------------------------------------------------
images = strcat([imdb.imageDir '/'], imdb.images.name(batch)) ;

isVal = ~isempty(batch) && imdb.images.set(batch(1)) ~= 1;

if ~isVal
    im = imdb_get_batch(images, opts, ...
        'prefetch', nargout == 0);
else
    im = imdb_get_batch(images, opts, ...
        'prefetch', nargout == 0, ...
        'transformation', 'none');
end

labels = imdb.images.label(batch) ;


function inputs = getBatchDagNN(imdb, batch, opts, useGpu)
% -------------------------------------------------------------------------
images = strcat([imdb.imageDir '/'], imdb.images.name(batch)) ;

isVal = ~isempty(batch) && imdb.images.set(batch(1)) ~= 1;

if ~isVal
    im = imdb_get_batch(images, opts, ...
        'prefetch', nargout == 0);
else
    im = imdb_get_batch(images, opts, ...
        'prefetch', nargout == 0, ...
        'transformation', 'none');
end

if nargout > 0
  if useGpu
    im = gpuArray(im) ;
  end
  labels = imdb.images.label(batch) ;
  inputs = {'input', im, 'label', labels} ;
end



% -------------------------------------------------------------------------
function net = initializeCNNNetwork(imdb, opts)
% -------------------------------------------------------------------------

numClass = length(imdb.classes.name);
if opts.fromScratch
% if false
% TO DO
    net.layers = vgg_m(numClass);
else
    
    net = load(fullfile('data/models', opts.model)); % Load model if specified
    
    % net.normalization.keepAspect = opts.keepAspect;
    fprintf('Initializing from model: %s\n', opts.model);
    
    
    if isfield(net, 'params')
       net = dagnn.DagNN.loadobj(net);
    end
    isDag = isa(net, 'dagnn.DagNN');
    
    if isDag
        net = initializeCNN_dagNN(net, numClass);
    else
        net = initializeCNN_simpleNN(net, numClass);
    end
    
    
end
% Rename classes
net.meta.classes.name = imdb.classes.name;
net.meta.classes.description = imdb.classes.name;


% Other details
net.meta.normalization.imageSize = [224, 224, 3] ;
% net.meta.normalization.interpolation = 'bicubic' ;
% net.meta.normalization.border = 256 - net.normalization.imageSize(1:2) ;
% net.meta.normalization.averageImage = [] ;
net.meta.normalization.keepAspect = opts.keepAspect ;
net.meta.augmentation.rgbVariance = [];
net.meta.augmentation.transformation = opts.dataAugmentation{1};


function net = initializeCNN_simpleNN(net, numClass)

% backup the fully connected layers
tempLayers = net.layers(end-3:end);
net.layers = net.layers(1:end-4);

% Add dropout layer after fully connected layers
net = add_dropout(net, '6');

net.layers{end+1} = tempLayers{1};
net.layers{end+1} = tempLayers{2};

net = add_dropout(net, '7');


% Initial the last but one layer with random weights
wopts.scale = 1;
wopts.weightInitMethod = 'gaussian';
net.layers{end+1} = struct('type', 'conv', 'name', 'fc8', ...
    'weights', {{init_weight(wopts, 1, 1, 4096, numClass, 'single'), zeros(numClass, 1, 'single')}}, ...
    'stride', 1, ...
    'pad', 0, ...
    'learningRate', [10 20], ...
    'weightDecay', [1 0]) ;


% Last layer is softmaxloss (switch to softmax for prediction)
net.layers{end+1} = struct('type', 'softmaxloss', 'name', 'loss') ;


function net = initializeCNN_dagNN(net, numClass)

pidx = net.getParamIndex('fc1000_filter');
fc_input_dim = size(net.params(pidx).value, 3);
net.removeLayer({'fc1000', 'prob'});

wopts.scale = 1;
wopts.weightInitMethod = 'gaussian';
params(1).value = init_weight(wopts, 1, 1, fc_input_dim, numClass, 'single');
params(1).name = 'fclayer_filter';
params(2).value = zeros(numClass, 1, 'single');
params(2).name = 'fclayer_bias';
net.addLayer('fclayer', dagnn.Conv(), 'pool5', 'fcfeat', {params(1).name params(2).name})
for f = 1:2,
    varId = net.getParamIndex(params(f).name);
    net.params(varId).value = params(f).value;
    net.params(varId).learningRate = 10;
    net.params(varId).weightDecay = 10;
end

inputName = net.getInputs();
if ~strcmp(inputName, 'input')
    net.renameVar(inputName, 'input')
end

net.addLayer('losslayer', dagnn.Loss('loss', 'softmaxlog'), ...
    {'fcfeat', 'label'}, 'objective') ;

net.addLayer('top1err', dagnn.Loss('loss', 'classerror'), ...
    {'fcfeat','label'}, 'top1err') ;
net.addLayer('top5err', dagnn.Loss('loss', 'topkerror', ...
    'opts', {'topK',5}), ...
    {'fcfeat','label'}, 'top5err') ;




function weights = init_weight(opts, h, w, in, out, type)
% -------------------------------------------------------------------------
% See K. He, X. Zhang, S. Ren, and J. Sun. Delving deep into
% rectifiers: Surpassing human-level performance on imagenet
% classification. CoRR, (arXiv:1502.01852v1), 2015.

switch lower(opts.weightInitMethod)
  case 'gaussian'
    sc = 0.01/opts.scale ;
    weights = randn(h, w, in, out, type)*sc;
  case 'xavier'
    sc = sqrt(3/(h*w*in)) ;
    weights = (rand(h, w, in, out, type)*2 - 1)*sc ;
  case 'xavierimproved'
    sc = sqrt(2/(h*w*out)) ;
    weights = randn(h, w, in, out, type)*sc ;
  otherwise
    error('Unknown weight initialization method''%s''', opts.weightInitMethod) ;
end

function net = add_dropout(net, id)
% --------------------------------------------------------------------

net.layers{end+1} = struct('type', 'dropout', ...
                           'name', sprintf('dropout%s', id), ...
                           'rate', 0.5) ;


% -------------------------------------------------------------------------
function layers = vgg_m(numClass)
% -------------------------------------------------------------------------
% Initial model randomly
layers = {} ;
scal = 1 ;
init_bias = 0.1;

% Block 1
layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(7, 7, 3, 96, 'single'), ...
                           'biases', zeros(1, 96, 'single'), ...
                           'stride', 2, ...
                           'pad', 0, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
layers{end+1} = struct('type', 'relu') ;
layers{end+1} = struct('type', 'normalize', ...
                           'param', [5 1 0.0001/5 0.75]) ;                       
layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;


% Block 2
layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(5, 5, 96, 256, 'single'), ...
                           'biases', init_bias*ones(1, 256, 'single'), ...
                           'stride', 2, ...
                           'pad', 1, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
layers{end+1} = struct('type', 'relu') ;
layers{end+1} = struct('type', 'normalize', ...
                           'param', [5 1 0.0001/5 0.75]) ;                       
layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', [0 1 0 1]) ;


% Block 3
layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(3,3,256,512,'single'), ...
                           'biases', init_bias*ones(1,512,'single'), ...
                           'stride', 1, ...
                           'pad', 1, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
layers{end+1} = struct('type', 'relu') ;

% Block 4
layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(3,3,512,512,'single'), ...
                           'biases', init_bias*ones(1,512,'single'), ...
                           'stride', 1, ...
                           'pad', 1, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
layers{end+1} = struct('type', 'relu') ;

% Block 5
layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(3,3,512,512,'single'), ...
                           'biases', init_bias*ones(1,512,'single'), ...
                           'stride', 1, ...
                           'pad', 1, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
layers{end+1} = struct('type', 'relu') ;
layers{end+1} = struct('type', 'pool', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;

% Block 6
layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(6,6,512,4096,'single'),...
                           'biases', init_bias*ones(1,4096,'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
layers{end+1} = struct('type', 'relu') ;
layers{end+1} = struct('type', 'dropout', ...
                           'rate', 0.5) ;

% Block 7
layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(1,1,4096,4096,'single'),...
                           'biases', init_bias*ones(1,4096,'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;
layers{end+1} = struct('type', 'relu') ;
layers{end+1} = struct('type', 'dropout', ...
                           'rate', 0.5) ;

% Block 8
layers{end+1} = struct('type', 'conv', ...
                           'filters', 0.01/scal * randn(1,1,4096,numClass,'single'), ...
                           'biases', zeros(1, numClass, 'single'), ...
                           'stride', 1, ...
                           'pad', 0, ...
                           'filtersLearningRate', 1, ...
                           'biasesLearningRate', 2, ...
                           'filtersWeightDecay', 1, ...
                           'biasesWeightDecay', 0) ;

layers{end+1} = struct('type', 'softmaxloss') ;
