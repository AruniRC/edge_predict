% -------------------------------------------------------------------------
function imdb = imagenet100k_get_imdb(dataDir)
% -------------------------------------------------------------------------
% Get IMDB for ImageNet-based 100k dataset

% dataDir = '/data/arunirc/datasets/ImageNet/images/';


imdb.imageDir = fullfile(dataDir);
flag = 0;

% Training set
if exist(fullfile(dataDir, 'train'))

    fid = fopen(fullfile(dataDir, 'files_100k_train.txt'));    
    if fid < 1
       error( 'files_100k_train.txt not found under `dataDir`!');
    end
    
    trainFileNames = textscan(fid,'%s','Delimiter','\n');
    trainFileNames = trainFileNames{1};
    imdb.images.name = trainFileNames;
    imdb.images.set = ones(1, length(trainFileNames));
    flag = flag + 1;
else
    warning(sprintf('train folder does not exist under `dataDir` %s', dataDir));
end


% Val set
if exist(fullfile(dataDir, 'val'))
    
    fid = fopen(fullfile(dataDir, 'files_100k_val.txt'));    
    if fid < 1
       error( 'files_100k_val.txt not found under dataDir!');
    end    
    
    valFileNames = textscan(fid,'%s','Delimiter','\n');
    valFileNames = valFileNames{1};
    imdb.images.name = vertcat(imdb.images.name, valFileNames);
    imdb.images.set = horzcat(imdb.images.set, 2*ones(1, length(valFileNames)));
    flag = flag + 1;
else
    warning(sprintf('val folder does not exist under dataDir %s', dataDir));
end

if flag == 0
    error('No train or validation folders found!');
end

% Create ids
imdb.images.id = 1:numel(imdb.images.set);

% Class info
imdb.classes.name = arrayfun(@(x)sprintf('%d',x),1:4,'UniformOutput',false) ;

% Clean-up
% system('rm train_file_list.txt');
% system('val_file_list.txt');

 
