classdef GlobalPooling  < dagnn.ElementWise
    %GLOBALPOOLING Global Average Pooling (GAP) layer
    %	Sum-pools over all the spatial locations of a feature map.
    %   An hxwxk feature map is reduced to 1x1xk.
    %   Wrapper for the vl_gap() method.
    
    properties
        
    end
    
    methods
        
        function outputs = forward(obj, inputs, params)
          outputs{1} = vl_gap(inputs{1}, []) ;
        end

        function [derInputs, derParams] = backward(obj, inputs, params, derOutputs)
          [derInputs{1}] = vl_gap(inputs{1}, derOutputs{1}) ;
          derParams = {} ;
        end       
        
        function rfs = getReceptiveFields(obj)
          rfs.size = [75 75] ;
          rfs.stride = [1 1] ;
          rfs.offset = [1 1] ;
        end

        function obj = GlobalPooling(varargin)
          obj.load(varargin) ;
        end
        
        
    end
    
end

