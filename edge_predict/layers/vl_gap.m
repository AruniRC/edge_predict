function y = vl_gap(x, dzdy)
%VL_GAP Global-Average-Pooling (GAP) layer
%	Pools over all the spatial locations of a feature map

backMode = ~isempty(dzdy);

sz = size(x);

% basic validation
assert(isnumeric(x));
assert(ndims(x)>2); % may be 4-D array (WxHxKxN) or have a single image N=1

if backMode
    % derivatives
    y = 1/prod(sz(1:2)) * repmat(dzdy, [size(x,1) size(x,2) 1 1]);
else
    % forward pass
    %	- average pool over the spatial dimensions
    y = 1/prod(sz(1:2)) * sum(sum(x,1),2);
end