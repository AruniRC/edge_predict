% -------------------------------------------------------------------------
function inputs = getBatchDagNN(imdb, batch, opts, useGpu, useBox)
% -------------------------------------------------------------------------

if nargin < 5
    useBox = false;
end

if useBox
    assert(isfield(imdb.images, 'bounds'), 'no bounding boxes information in imdb');
end

images = strcat([imdb.imageDir filesep], imdb.images.name(batch)) ;
bbox = [];
if isfield(imdb.images, 'bounds') && useBox
    bbox = imdb.images.bounds(:, batch);
end

isVal = ~isempty(batch) && imdb.images.set(batch(1)) ~= 1;

if ~isVal
    [im, mask] = imdb_get_batch_bcnn(images, opts, ...
                            'prefetch', nargout == 0, 'bbox', bbox);
else
    [im, mask] = imdb_get_batch_bcnn(images, opts, ...
                            'prefetch', nargout == 0, 'transformation', 'none', 'bbox', bbox);
end


labels = imdb.images.label(batch) ;
numAugments = size(im{1},4)/numel(batch);

labels = reshape(repmat(labels, numAugments, 1), 1, size(im{1},4));

if nargout > 0
  if useGpu
      for i=1:numel(im)
          im{i} = gpuArray(im{i}) ;
      end
%     im1 = gpuArray(im{1}) ;
%     im2 = gpuArray(im{2}) ;
%   else
%       im1 = im{1};
%       im2 = im{2};
  end
  
  if numel(im) == 1
      inputs = {'input', im{1}} ;
  else
      inputs = {'input', im{1}, 'netb_input', im{2}} ;
  end
  
  if useBox
      inputs{end+1} = 'mask';
      inputs{end+1} = mask;
  end
  
  inputs{end+1} = 'label';
  inputs{end+1} = labels;
  
end

