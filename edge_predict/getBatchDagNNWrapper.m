% -------------------------------------------------------------------------
function fn = getBatchDagNNWrapper(opts, useGpu, useBox)
% -------------------------------------------------------------------------
if nargin < 3
    useBox = false;
end
if opts.videoPool
    fn = @(imdb,batch) getBatchDagNNVideo(imdb, batch, opts, useGpu) ;
else
    fn = @(imdb,batch) getBatchDagNN(imdb,batch,opts,useGpu, useBox) ;
end

