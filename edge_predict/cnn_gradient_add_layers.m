function net = cnn_gradient_add_layers(net, varargin)
% CNN_GRADIENT_ADD_LAYERS Adds new layers to pre-trained edge 
% prediction network
%
% Input
% -----	
%	net  	Pre-trained network for missing edge prediction. 
%			Will expect single-channel edge-map as input.
% Output
% ------
%	net 	Same network with layers added. Takes in RGB 
%			image in the first layer.

opts.scale = 1 ;
opts.initBias = 0.1 ;
opts.weightDecay = 1 ;
opts.weightInitMethod = 'xavierimproved' ;
% opts.weightInitMethod = 'gaussian' ;
opts.model = 'edge-net' ;
opts.batchNormalization = false ;
opts.networkType = 'simplenn' ;
opts.cudnnWorkspaceLimit = 1024*1024*1204 ; % 1GB

% -------------------------------------------------------------------------
% Edge-net options
opts.numChannel = 8;
opts.numClass = 4;

opts = vl_argparse(opts, varargin) ;


% Repeat the weights of the initial layer to accept 32-channel 
% input
weight_1 = net.params(1).value ;
weight_1_rep = repmat(weight_1, [1 1 32 1]);
net.params(1).value = weight_1_rep;


% create a layer for 3-channel RBG images as input
% - 32 channels at the output of a 3x3 convolution
% -	input: 'input'
% -	output: 'x0'
img_conv = dagnn.Conv('size',[3 3 3 32],'pad',1,'stride',1,'hasBias',true);
net.addLayer('im_conv', img_conv, {'input'}, {'x0'}, {'im_conv_f', 'im_conv_b'});
net.params(net.getParamIndex('im_conv_f')).value =  0.01/opts.scale*randn(3, 3, 3, 32, 'single');
net.params(net.getParamIndex('im_conv_f')).learningRate = 1;
net.params(net.getParamIndex('im_conv_b')).value = ones(32, 1, 'single')*opts.initBias;
net.params(net.getParamIndex('im_conv_b')).learningRate = 1; 

% change the next layers I/O
% - input: 'x0'
% - output remains 'x1' as before
net.layers(net.getLayerIndex('conv1_1')).inputs = {'x0'} ;




