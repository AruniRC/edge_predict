% -------------------------------------------------------------------------
function imdb = edge_get_imdb(dataDir)
% -------------------------------------------------------------------------
% Get IMDB for ImageNet-based edge-response datasets

% dataDir = '/data/arunirc/datasets/ImageNet-hog/';


imdb.imageDir = fullfile(dataDir);
flag = 0;

% Training set
if exist(fullfile(dataDir, 'train'))
    cmd = sprintf('find %s -type f > train_file_list.txt', fullfile(dataDir, 'train'));
    system(cmd)
    fid = fopen('train_file_list.txt');
    trainFileNames = textscan(fid,'%s','Delimiter','\n');
    trainFileNames = trainFileNames{1};
    fn = cellfun(@(x)(strsplit(x, '/')), trainFileNames, 'un', 0);
    fnTrain = cellfun(@(x)(strjoin(x(end-2:end), '/')), fn, 'un', 0); 
    imdb.images.name = fnTrain;
    imdb.images.set = ones(1, length(fnTrain));
    flag = flag + 1;
else
    warning(sprintf('train folder does not exist under dataDir %s', dataDir));
end


% Val set
if exist(fullfile(dataDir, 'val'))
    cmd = sprintf('find %s -type f > val_file_list.txt', fullfile(dataDir, 'val'));
    system(cmd)
    fid = fopen('val_file_list.txt');
    valFileNames = textscan(fid,'%s','Delimiter','\n');
    valFileNames = valFileNames{1};
    fn = cellfun(@(x)(strsplit(x, '/')), valFileNames, 'un', 0);
    fnVal = cellfun(@(x)(strjoin(x(end-1:end), '/')), fn, 'un', 0); 
    imdb.images.name = vertcat(imdb.images.name, fnVal);
    imdb.images.set = horzcat(imdb.images.set, 2*ones(1, length(fnVal)));
    flag = flag + 1;
else
    warning(sprintf('val folder does not exist under dataDir %s', dataDir));
end

if flag == 0
    error('No train or validation folders found!');
end

% Create ids
imdb.images.id = 1:numel(imdb.images.set);

% Class info
imdb.classes.name = arrayfun(@(x)sprintf('%d',x),1:4,'UniformOutput',false) ;

% Clean-up
% system('rm train_file_list.txt');
% system('val_file_list.txt');

 
