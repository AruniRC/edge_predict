function net = cnn_edge_init(varargin)
% CNN_IMAGENET_INIT  Initialize a standard CNN for ImageNet

opts.scale = 1 ;
opts.initBias = 0.1 ;
opts.weightDecay = 1 ;
opts.weightInitMethod = 'xavierimproved' ;
% opts.weightInitMethod = 'gaussian' ;
opts.model = 'edge-net' ;
opts.batchNormalization = false ;
opts.networkType = 'simplenn' ;
opts.cudnnWorkspaceLimit = 1024*1024*1204 ; % 1GB

% -------------------------------------------------------------------------
% Edge-net options
opts.numChannel = 8;
opts.numClass = 4;

opts = vl_argparse(opts, varargin) ;

% Define layers
switch opts.model
  case 'alexnet'
    net.meta.normalization.imageSize = [227, 227, 3] ;
    net = alexnet(net, opts) ;
    bs = 256 ;
  case 'vgg-f'
    net.meta.normalization.imageSize = [224, 224, 3] ;
    net = vgg_f(net, opts) ;
    bs = 256 ;
  case 'vgg-m'
    net.meta.normalization.imageSize = [224, 224, 3] ;
    net = vgg_m(net, opts) ;
    bs = 196 ;
  case 'vgg-s'
    net.meta.normalization.imageSize = [224, 224, 3] ;
    net = vgg_s(net, opts) ;
    bs = 128 ;
  case 'vgg-vd-16'
    net.meta.normalization.imageSize = [224, 224, 3] ;
    net = vgg_vd(net, opts) ;
    bs = 32 ;
  case 'vgg-vd-19'
    net.meta.normalization.imageSize = [224, 224, 3] ;
    net = vgg_vd(net, opts) ;
    bs = 24 ;
  case 'edge-net'
    net.meta.normalization.imageSize = [224, 224, opts.numChannel] ;
    net = edge_net(net, opts) ;
    bs = 512 ;      
  case 'alex-filter-net'  
    net.meta.normalization.imageSize = [227, 227, 3] ;
    net = edge_alexnet_filter(net, opts) ;
    bs = 256 ;  
    
  case 'rectangle-filter'  
    net.meta.normalization.imageSize = [100, 100, 1] ;
    opts.numChannel = 1;
    opts.numClass = 1;
    net = edge_rect_filter(net, opts) ;
    bs = 180 ;
  
  case 'gradient-filter'  
    net.meta.normalization.imageSize = [100, 100, 1] ;
    opts.numChannel = 1;
    opts.numClass = 1;
    net = edge_rect_filter(net, opts) ;
    bs = 100 ;

  % -----------------------------------------------------------------------
  case 'edge-occlusion'
    net.meta.normalization.imageSize = [200, 200, 1] ;
    opts.numChannel = 1;
    opts.numClass = 1;
    net = edge_occluder_net(net, opts) ;
    bs = 128 ; 

  case 'edge-occlusion-wider'
  	net.meta.normalization.imageSize = [200, 200, 1] ;
    opts.numChannel = 1;
    opts.numClass = 1;
  	net = edge_occluder_net_wider(net, opts) ;
  	bs = 32 ; 

  case 'edge-occlusion-deeper'
  	net.meta.normalization.imageSize = [200, 200, 1] ;
    opts.numChannel = 1;
    opts.numClass = 1;
  	net = edge_occluder_net_deeper(net, opts) ;
  	bs = 32 ;	  

  case 'edge-occlusion-small-filter'
  	net.meta.normalization.imageSize = [200, 200, 1] ;
    opts.numChannel = 1;
    opts.numClass = 1;
  	net = edge_occluder_net_small_filter(net, opts) ;
  	bs = 128 ;
  	  
  case 'edge-u-net'
    net.meta.normalization.imageSize = [200, 200, 1] ;
    opts.numChannel = 1;
    opts.numClass = 1;
    net = edge_occluder_unet(net, opts) ;
    bs = 128 ; 
    
  % -----------------------------------------------------------------------

  otherwise
    error('Unknown model ''%s''', opts.model) ;
end

% final touches
switch lower(opts.weightInitMethod)
  case {'xavier', 'xavierimproved'}
    net.layers{end}.weights{1} = net.layers{end}.weights{1} / 10 ;
end
net.layers{end+1} = struct('type', 'softmaxloss', 'name', 'loss') ;


% Meta parameters
net.meta.inputSize = net.meta.normalization.imageSize ;
net.meta.normalization.border = 256 - net.meta.normalization.imageSize(1:2) ;
net.meta.normalization.interpolation = 'bilinear' ;
net.meta.normalization.averageImage = [] ;
net.meta.normalization.keepAspect = true ;
net.meta.augmentation.rgbVariance = zeros(0,3) ;
net.meta.augmentation.transformation = 'stretch' ;

if ~opts.batchNormalization
  lr = logspace(-2, -4, 60) ;
else
  lr = 0.0001 * logspace(-1, -4, 20) ;  % change number of epochs
end

net.meta.trainOpts.learningRate = lr ;
net.meta.trainOpts.numEpochs = numel(lr) ;
net.meta.trainOpts.batchSize = bs ;
net.meta.trainOpts.weightDecay = 0.0005 ;

% Fill in default values
net = vl_simplenn_tidy(net) ;

% Switch to DagNN if requested
switch lower(opts.networkType)
  case 'simplenn'
    % done
  case 'dagnn'
    net = dagnn.DagNN.fromSimpleNN(net, 'canonicalNames', true) ;   
    net.addLayer('top1err', dagnn.Loss('loss', 'classerror'), ...
                 {'prediction','label'}, 'top1err') ;            
    net.addLayer('top5err', dagnn.Loss('loss', 'topkerror','opts', {'topK',2}), ...
                 {'prediction','label'}, 'top5err') ;

    % modify network loss function     
    if isequal(opts.model, 'rectangle-filter') || ...
                isequal(opts.model, 'edge-occlusion') || ...
                isequal(opts.model, 'edge-u-net') || ...
                isequal(opts.model, 'edge-occlusion-wider')
            
        % binary labels
        net.removeLayer('loss');
        net.addLayer('loss', dagnn.Loss('loss', 'logistic'), {'prediction','label'}, 'objective');  
        
        net.removeLayer('top1err');
        net.removeLayer('top5err');
        
        net.addLayer('err', dagnn.Loss('loss', 'binaryerror'), ...
                 {'prediction','label'}, 'err') ;
    end   

    if isequal(opts.model, 'gradient-filter')
    	% use L2-loss for gradient images
        net.removeLayer('loss');	
        net.removeLayer('top1err');
        net.removeLayer('top5err');
        net.addLayer('l2_loss', L2Loss(), {'prediction','label'}, {'objective'});
        % net.meta.trainOpts.learningRate = 10*(net.meta.trainOpts.learningRate);
    end

    
    

  otherwise
    assert(false) ;
end

% --------------------------------------------------------------------
function net = add_block(net, opts, id, h, w, in, out, stride, pad)
% --------------------------------------------------------------------
info = vl_simplenn_display(net) ;
fc = (h == info.dataSize(1,end) && w == info.dataSize(2,end)) ;
if fc
  name = 'fc' ;
else
  name = 'conv' ;
end
convOpts = {'CudnnWorkspaceLimit', opts.cudnnWorkspaceLimit} ;
net.layers{end+1} = struct('type', 'conv', 'name', sprintf('%s%s', name, id), ...
                           'weights', {{init_weight(opts, h, w, in, out, 'single'), ...
                             ones(out, 1, 'single')*opts.initBias}}, ...
                           'stride', stride, ...
                           'pad', pad, ...
                           'learningRate', [1 2], ...
                           'weightDecay', [opts.weightDecay 0], ...
                           'opts', {convOpts}) ;
if opts.batchNormalization
  net.layers{end+1} = struct('type', 'bnorm', 'name', sprintf('bn%s',id), ...
                             'weights', {{ones(out, 1, 'single'), zeros(out, 1, 'single'), ...
                               zeros(out, 2, 'single')}}, ...
                             'learningRate', [2 1 0.3], ...
                             'weightDecay', [0 0]) ;
end
net.layers{end+1} = struct('type', 'relu', 'name', sprintf('relu%s',id), 'leak', 0) ;

% -------------------------------------------------------------------------
function weights = init_weight(opts, h, w, in, out, type)
% -------------------------------------------------------------------------
% See K. He, X. Zhang, S. Ren, and J. Sun. Delving deep into
% rectifiers: Surpassing human-level performance on imagenet
% classification. CoRR, (arXiv:1502.01852v1), 2015.

switch lower(opts.weightInitMethod)
  case 'gaussian'
    sc = 0.01/opts.scale ;
    weights = randn(h, w, in, out, type)*sc;
  case 'xavier'
    sc = sqrt(3/(h*w*in)) ;
    weights = (rand(h, w, in, out, type)*2 - 1)*sc ;
  case 'xavierimproved'
    sc = sqrt(2/(h*w*out)) ;
    weights = randn(h, w, in, out, type)*sc ;
  otherwise
    error('Unknown weight initialization method''%s''', opts.weightInitMethod) ;
end

% --------------------------------------------------------------------
function net = add_norm(net, opts, id)
% --------------------------------------------------------------------
if ~opts.batchNormalization
  net.layers{end+1} = struct('type', 'normalize', ...
                             'name', sprintf('norm%s', id), ...
                             'param', [5 1 0.0001/5 0.75]) ;
end

% --------------------------------------------------------------------
function net = add_dropout(net, opts, id)
% --------------------------------------------------------------------
if ~opts.batchNormalization
  net.layers{end+1} = struct('type', 'dropout', ...
                             'name', sprintf('dropout%s', id), ...
                             'rate', 0.5) ;
end


% --------------------------------------------------------------------
function net = alexnet(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;

net = add_block(net, opts, '1', 11, 11, 3, 96, 4, 0) ;
net = add_norm(net, opts, '1') ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool1', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;


net = add_block(net, opts, '2', 5, 5, 48, 256, 1, 2) ;
net = add_norm(net, opts, '2') ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool2', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;


net = add_block(net, opts, '3', 3, 3, 256, 384, 1, 1) ;
net = add_block(net, opts, '4', 3, 3, 192, 384, 1, 1) ;
net = add_block(net, opts, '5', 3, 3, 192, 256, 1, 1) ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool5', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;

net = add_block(net, opts, '6', 6, 6, 256, 4096, 1, 0) ;
net = add_dropout(net, opts, '6') ;

net = add_block(net, opts, '7', 1, 1, 4096, 4096, 1, 0) ;
net = add_dropout(net, opts, '7') ;

net = add_block(net, opts, '8', 1, 1, 4096, 1000, 1, 0) ;
net.layers(end) = [] ;
if opts.batchNormalization, net.layers(end) = [] ; end

% --------------------------------------------------------------------
function net = vgg_s(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;
net = add_block(net, opts, '1', 7, 7, 3, 96, 2, 0) ;
net = add_norm(net, opts, '1') ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool1', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 3, ...
                           'pad', [0 2 0 2]) ;

net = add_block(net, opts, '2', 5, 5, 96, 256, 1, 0) ;
net = add_norm(net, opts, '2') ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool2', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', [0 1 0 1]) ;

net = add_block(net, opts, '3', 3, 3, 256, 512, 1, 1) ;
net = add_block(net, opts, '4', 3, 3, 512, 512, 1, 1) ;
net = add_block(net, opts, '5', 3, 3, 512, 512, 1, 1) ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool5', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 3, ...
                           'pad', [0 1 0 1]) ;

net = add_block(net, opts, '6', 6, 6, 512, 4096, 1, 0) ;
net = add_dropout(net, opts, '6') ;

net = add_block(net, opts, '7', 1, 1, 4096, 4096, 1, 0) ;
net = add_dropout(net, opts, '7') ;

net = add_block(net, opts, '8', 1, 1, 4096, 1000, 1, 0) ;
net.layers(end) = [] ;
if opts.batchNormalization, net.layers(end) = [] ; end

% --------------------------------------------------------------------
function net = vgg_m(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;
net = add_block(net, opts, '1', 7, 7, 3, 96, 2, 0) ;
net = add_norm(net, opts, '1') ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool1', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;

net = add_block(net, opts, '2', 5, 5, 96, 256, 2, 1) ;
net = add_norm(net, opts, '2') ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool2', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', [0 1 0 1]) ;

net = add_block(net, opts, '3', 3, 3, 256, 512, 1, 1) ;
net = add_block(net, opts, '4', 3, 3, 512, 512, 1, 1) ;
net = add_block(net, opts, '5', 3, 3, 512, 512, 1, 1) ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool5', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;

net = add_block(net, opts, '6', 6, 6, 512, 4096, 1, 0) ;
net = add_dropout(net, opts, '6') ;

net = add_block(net, opts, '7', 1, 1, 4096, 4096, 1, 0) ;
net = add_dropout(net, opts, '7') ;

net = add_block(net, opts, '8', 1, 1, 4096, 1000, 1, 0) ;
net.layers(end) = [] ;
if opts.batchNormalization, net.layers(end) = [] ; end

% --------------------------------------------------------------------
function net = vgg_f(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;
net = add_block(net, opts, '1', 11, 11, 3, 64, 4, 0) ;
net = add_norm(net, opts, '1') ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool1', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', [0 1 0 1]) ;

net = add_block(net, opts, '2', 5, 5, 64, 256, 1, 2) ;
net = add_norm(net, opts, '2') ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool2', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;

net = add_block(net, opts, '3', 3, 3, 256, 256, 1, 1) ;
net = add_block(net, opts, '4', 3, 3, 256, 256, 1, 1) ;
net = add_block(net, opts, '5', 3, 3, 256, 256, 1, 1) ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool5', ...
                           'method', 'max', ...
                           'pool', [3 3], ...
                           'stride', 2, ...
                           'pad', 0) ;

net = add_block(net, opts, '6', 6, 6, 256, 4096, 1, 0) ;
net = add_dropout(net, opts, '6') ;

net = add_block(net, opts, '7', 1, 1, 4096, 4096, 1, 0) ;
net = add_dropout(net, opts, '7') ;

net = add_block(net, opts, '8', 1, 1, 4096, 1000, 1, 0) ;
net.layers(end) = [] ;
if opts.batchNormalization, net.layers(end) = [] ; end

% --------------------------------------------------------------------
function net = vgg_vd(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;
net = add_block(net, opts, '1_1', 3, 3, 3, 64, 1, 1) ;
net = add_block(net, opts, '1_2', 3, 3, 64, 64, 1, 1) ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool1', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;

net = add_block(net, opts, '2_1', 3, 3, 64, 128, 1, 1) ;
net = add_block(net, opts, '2_2', 3, 3, 128, 128, 1, 1) ;
net.layers{end+1} = struct('type', 'pool', 'name', 'pool2', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;

net = add_block(net, opts, '3_1', 3, 3, 128, 256, 1, 1) ;
net = add_block(net, opts, '3_2', 3, 3, 256, 256, 1, 1) ;
net = add_block(net, opts, '3_3', 3, 3, 256, 256, 1, 1) ;
if strcmp(opts.model, 'vgg-vd-19')
  net = add_block(net, opts, '3_4', 3, 3, 256, 256, 1, 1) ;
end
net.layers{end+1} = struct('type', 'pool', 'name', 'pool3', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;

net = add_block(net, opts, '4_1', 3, 3, 256, 512, 1, 1) ;
net = add_block(net, opts, '4_2', 3, 3, 512, 512, 1, 1) ;
net = add_block(net, opts, '4_3', 3, 3, 512, 512, 1, 1) ;
if strcmp(opts.model, 'vgg-vd-19')
  net = add_block(net, opts, '4_4', 3, 3, 512, 512, 1, 1) ;
end
net.layers{end+1} = struct('type', 'pool', 'name', 'pool4', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;

net = add_block(net, opts, '5_1', 3, 3, 512, 512, 1, 1) ;
net = add_block(net, opts, '5_2', 3, 3, 512, 512, 1, 1) ;
net = add_block(net, opts, '5_3', 3, 3, 512, 512, 1, 1) ;
if strcmp(opts.model, 'vgg-vd-19')
  net = add_block(net, opts, '5_4', 3, 3, 512, 512, 1, 1) ;
end
net.layers{end+1} = struct('type', 'pool', 'name', 'pool5', ...
                           'method', 'max', ...
                           'pool', [2 2], ...
                           'stride', 2, ...
                           'pad', 0) ;

net = add_block(net, opts, '6', 7, 7, 512, 4096, 1, 0) ;
net = add_dropout(net, opts, '6') ;

net = add_block(net, opts, '7', 1, 1, 4096, 4096, 1, 0) ;
net = add_dropout(net, opts, '7') ;

net = add_block(net, opts, '8', 1, 1, 4096, 1000, 1, 0) ;
net.layers(end) = [] ;
if opts.batchNormalization, net.layers(end) = [] ; end



% --------------------------------------------------------------------
function net = edge_net(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;


net = add_block(net, opts, '1_1', 3, 3, opts.numChannel, 64, 1, 1) ;
net = add_block(net, opts, '1_2', 3, 3, 64, 64, 2, 1) ;


net = add_block(net, opts, '2_1', 3, 3, 64, 128, 1, 1) ;
net = add_block(net, opts, '2_2', 3, 3, 128, 128, 2, 1) ;

                       
net = add_block(net, opts, '3_1', 3, 3, 128, 256, 1, 1) ;
net = add_block(net, opts, '3_2', 3, 3, 256, 256, 2, 1) ;

  
% upsampling layers
net.layers{end+1} = struct('type', 'convt', 'name', 'upsample_1', ...
                           'weights', {{ single(bilinear_u(3, 1, 256)), ...
                                      []}}, ...
                           'upsample', 2, ...
                           'crop', 1, ...
                           'learningRate', [0 0], ...
                           'weightDecay', [0 0]) ;
net = add_block(net, opts, '4_1', 3, 3, 256, 256, 1, 1) ;
net = add_block(net, opts, '4_2', 3, 3, 256, 128, 1, 1) ;


net.layers{end+1} = struct('type', 'convt', 'name', 'upsample_2', ...
                           'weights', {{ single(bilinear_u(3, 1, 128)), ...
                                      []}}, ...
                           'upsample', 2, ...
                           'crop', [0 1 0 1], ...
                           'learningRate', [0 0], ...
                           'weightDecay', [0 0]) ;
net = add_block(net, opts, '5_1', 3, 3, 128, 128, 1, 1) ;
net = add_block(net, opts, '5_2', 3, 3, 128, 64, 1, 1) ;  


net.layers{end+1} = struct('type', 'convt', 'name', 'upsample_3', ...
                           'weights', {{ single(bilinear_u(3, 1, 64)), ...
                                      []}}, ...
                           'upsample', 2, ...
                           'crop', [0 1 0 1], ...
                           'learningRate', [0 0], ...
                           'weightDecay', [0 0]) ;
net = add_block(net, opts, '6_1', 3, 3, 64, 64, 1, 1) ;
net = add_block(net, opts, '6_2', 3, 3, 64, 32, 1, 1) ;  

% fc layer                       
net = add_block(net, opts, '7', 1, 1, 32, 32, 1, 0) ;
net = add_dropout(net, opts, '7'); 

% prediction scores layer
net = add_block(net, opts, '8', 1, 1, 32, opts.numClass, 1, 0) ;
net.layers(end) = [] ;

if opts.batchNormalization, net.layers(end) = [] ; end





% --------------------------------------------------------------------
function net = edge_alexnet_filter(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;


net = add_block(net, opts, '1_1', 3, 3, opts.numChannel, 64, 1, 1) ;
net = add_block(net, opts, '1_2', 3, 3, 64, 64, 2, 1) ;


net = add_block(net, opts, '2_1', 3, 3, 64, 128, 1, 1) ;
net = add_block(net, opts, '2_2', 3, 3, 128, 128, 2, 1) ;

                       
net = add_block(net, opts, '3_1', 3, 3, 128, 256, 1, 1) ;
net = add_block(net, opts, '3_2', 3, 3, 256, 256, 2, 1) ;

  
% upsampling layers
net.layers{end+1} = struct('type', 'convt', 'name', 'upsample_1', ...
                           'weights', {{ single(bilinear_u(3, 1, 256)), ...
                                      []}}, ...
                           'upsample', 2, ...
                           'crop', [0 1 0 1], ...
                           'learningRate', [0 0], ...
                           'weightDecay', [0 0]) ;
net = add_block(net, opts, '4_1', 3, 3, 256, 256, 1, 1) ;
net = add_block(net, opts, '4_2', 3, 3, 256, 128, 1, 1) ;


net.layers{end+1} = struct('type', 'convt', 'name', 'upsample_2', ...
                           'weights', {{ single(bilinear_u(3, 1, 128)), ...
                                      []}}, ...
                           'upsample', 2, ...
                           'crop', [0 1 0 1], ...
                           'learningRate', [0 0], ...
                           'weightDecay', [0 0]) ;
net = add_block(net, opts, '5_1', 3, 3, 128, 128, 1, 1) ;
net = add_block(net, opts, '5_2', 3, 3, 128, 64, 1, 1) ;  


net.layers{end+1} = struct('type', 'convt', 'name', 'upsample_3', ...
                           'weights', {{ single(bilinear_u(3, 1, 64)), ...
                                      []}}, ...
                           'upsample', 2, ...
                           'crop', [1 1 1 1], ...
                           'learningRate', [0 0], ...
                           'weightDecay', [0 0]) ;
net = add_block(net, opts, '6_1', 3, 3, 64, 64, 1, 1) ;
net = add_block(net, opts, '6_2', 3, 3, 64, 32, 1, 1) ;  

% fc layer                       
net = add_block(net, opts, '7', 1, 1, 32, 32, 1, 0) ;
net = add_dropout(net, opts, '7'); 

% prediction scores layer
net = add_block(net, opts, '8', 1, 1, 32, opts.numClass, 1, 0) ;
net.layers(end) = [] ;


if opts.batchNormalization, net.layers(end) = [] ; end






% --------------------------------------------------------------------
function net = edge_rect_filter(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;

% no decimating filters -- input size is maintained through network layers

net = add_block(net, opts, '1_1', 5, 5, opts.numChannel, 64, 2, 2) ;
net = add_block(net, opts, '1_2', 3, 3, 64, 32, 1, 1) ;

net = add_block(net, opts, '2_1', 5, 5, 32, 32, 1, 2) ;
net = add_block(net, opts, '2_2', 3, 3, 32, 64, 1, 1) ;
                       
net = add_block(net, opts, '3_1', 5, 5, 64, 128, 1, 2) ;
net = add_block(net, opts, '3_2', 3, 3, 128, 128, 1, 1) ;

net = add_block(net, opts, '4_1', 5, 5, 128, 128, 1, 2) ;
net = add_block(net, opts, '4_2', 3, 3, 128, 256, 1, 1) ;

% fc layer                       
net = add_block(net, opts, '7', 1, 1, 256, 512, 1, 0) ;
net = add_dropout(net, opts, '7'); 

% prediction scores layer
net = add_block(net, opts, '8', 1, 1, 512, opts.numClass, 1, 0) ;
% net.layers(end) = [] ;


if opts.batchNormalization, net.layers(end) = [] ; end



% --------------------------------------------------------------------
function net = edge_occluder_net(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;

% deeper network
net = add_block(net, opts, '1_1', 5, 5, opts.numChannel, 64, 2, 2) ;
net = add_block(net, opts, '1_2', 3, 3, 64, 32, 1, 1) ;

net = add_block(net, opts, '2_1', 5, 5, 32, 32, 1, 2) ;
net = add_block(net, opts, '2_2', 3, 3, 32, 64, 1, 1) ;
                       
net = add_block(net, opts, '3_1', 5, 5, 64, 128, 1, 2) ;
net = add_block(net, opts, '3_2', 3, 3, 128, 128, 1, 1) ;

net = add_block(net, opts, '4_1', 5, 5, 128, 128, 1, 2) ;
net = add_block(net, opts, '4_2', 3, 3, 128, 256, 1, 1) ;

% fc layer                       
net = add_block(net, opts, '7', 1, 1, 256, 512, 1, 0) ;
net = add_dropout(net, opts, '7'); 

% prediction scores layer
net = add_block(net, opts, '8', 1, 1, 512, opts.numClass, 1, 0) ;
net.layers(end) = [] ;


if opts.batchNormalization, net.layers(end) = [] ; end


% --------------------------------------------------------------------
function net = edge_occluder_net_small_filter(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;

% deeper network
net = add_block(net, opts, '1_1', 3, 3, opts.numChannel, 64, 2, 2) ;
net = add_block(net, opts, '1_2', 3, 3, 64, 32, 1, 1) ;

net = add_block(net, opts, '2_1', 3, 3, 32, 32, 1, 2) ;
net = add_block(net, opts, '2_2', 3, 3, 32, 64, 1, 1) ;
                       
net = add_block(net, opts, '3_1', 3, 3, 64, 128, 1, 2) ;
net = add_block(net, opts, '3_2', 3, 3, 128, 128, 1, 1) ;

net = add_block(net, opts, '4_1', 3, 3, 128, 128, 1, 2) ;
net = add_block(net, opts, '4_2', 3, 3, 128, 256, 1, 1) ;

% fc layer                       
net = add_block(net, opts, '7', 1, 1, 256, 512, 1, 0) ;
net = add_dropout(net, opts, '7'); 

% prediction scores layer
net = add_block(net, opts, '8', 1, 1, 512, opts.numClass, 1, 0) ;
net.layers(end) = [] ;


if opts.batchNormalization, net.layers(end) = [] ; end








% --------------------------------------------------------------------
function net = edge_occluder_net_wider(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;

% deeper network
net = add_block(net, opts, '1_1', 5, 5, opts.numChannel, 64, 2, 2) ;
net = add_block(net, opts, '1_2', 3, 3, 64, 128, 1, 1) ;

net = add_block(net, opts, '2_1', 5, 5, 128, 128, 1, 2) ;
net = add_block(net, opts, '2_2', 3, 3, 128, 256, 1, 1) ;
                       
net = add_block(net, opts, '3_1', 5, 5, 256, 256, 1, 2) ;
net = add_block(net, opts, '3_2', 3, 3, 256, 512, 1, 1) ;

net = add_block(net, opts, '4_1', 5, 5, 512, 512, 1, 2) ;
net = add_block(net, opts, '4_2', 3, 3, 512, 1024, 1, 1) ;

% fc layer                       
net = add_block(net, opts, '5', 1, 1, 1024, 2048, 1, 0) ;
net = add_dropout(net, opts, '5'); 

% prediction scores layer
net = add_block(net, opts, '6', 1, 1, 2048, opts.numClass, 1, 0) ;
net.layers(end) = [] ;

if opts.batchNormalization, net.layers(end) = [] ; end



% --------------------------------------------------------------------
function net = edge_occluder_net_deeper(net, opts)
% --------------------------------------------------------------------

net.layers = {} ;

% deeper network
net = add_block(net, opts, '1_1', 5, 5, opts.numChannel, 64, 2, 2) ;
net = add_block(net, opts, '1_2', 3, 3, 64, 32, 1, 1) ;

net = add_block(net, opts, '2_1', 5, 5, 32, 32, 1, 2) ;
net = add_block(net, opts, '2_2', 3, 3, 32, 64, 1, 1) ;
                       
net = add_block(net, opts, '3_1', 5, 5, 64, 128, 1, 2) ;
net = add_block(net, opts, '3_2', 3, 3, 128, 128, 1, 1) ;

net = add_block(net, opts, '4_1', 5, 5, 128, 128, 1, 2) ;
net = add_block(net, opts, '4_2', 3, 3, 128, 256, 1, 1) ;

net = add_block(net, opts, '5_1', 3, 3, 256, 256, 1, 2) ;
net = add_block(net, opts, '5_2', 3, 3, 256, 256, 1, 1) ;

net = add_block(net, opts, '6_1', 3, 3, 256, 256, 1, 2) ;
net = add_block(net, opts, '6_2', 3, 3, 256, 256, 1, 1) ;

% fc layer                       
net = add_block(net, opts, '7', 1, 1, 256, 512, 1, 0) ;
net = add_dropout(net, opts, '7'); 

% prediction scores layer
net = add_block(net, opts, '8', 1, 1, 512, opts.numClass, 1, 0) ;
net.layers(end) = [] ;


if opts.batchNormalization, net.layers(end) = [] ; end




% --------------------------------------------------------------------
function net = edge_unet(net, opts)
% --------------------------------------------------------------------
%   TODO 

net.layers = {} ;


net = add_block(net, opts, '1_1', 3, 3, opts.numChannel, 64, 1, 1) ;
net = add_block(net, opts, '1_2', 3, 3, 64, 64, 2, 1) ;


net = add_block(net, opts, '2_1', 3, 3, 64, 128, 1, 1) ;
net = add_block(net, opts, '2_2', 3, 3, 128, 128, 2, 1) ;

                       
net = add_block(net, opts, '3_1', 3, 3, 128, 256, 1, 1) ;
net = add_block(net, opts, '3_2', 3, 3, 256, 256, 2, 1) ;

  
% upsampling layers
net.layers{end+1} = struct('type', 'convt', 'name', 'upsample_1', ...
                           'weights', {{ single(bilinear_u(3, 1, 256)), ...
                                      []}}, ...
                           'upsample', 2, ...
                           'crop', [0 1 0 1], ...
                           'learningRate', [0 0], ...
                           'weightDecay', [0 0]) ;
net = add_block(net, opts, '4_1', 3, 3, 256, 256, 1, 1) ;
net = add_block(net, opts, '4_2', 3, 3, 256, 128, 1, 1) ;


net.layers{end+1} = struct('type', 'convt', 'name', 'upsample_2', ...
                           'weights', {{ single(bilinear_u(3, 1, 128)), ...
                                      []}}, ...
                           'upsample', 2, ...
                           'crop', [0 1 0 1], ...
                           'learningRate', [0 0], ...
                           'weightDecay', [0 0]) ;
net = add_block(net, opts, '5_1', 3, 3, 128, 128, 1, 1) ;
net = add_block(net, opts, '5_2', 3, 3, 128, 64, 1, 1) ;  


net.layers{end+1} = struct('type', 'convt', 'name', 'upsample_3', ...
                           'weights', {{ single(bilinear_u(3, 1, 64)), ...
                                      []}}, ...
                           'upsample', 2, ...
                           'crop', [1 1 1 1], ...
                           'learningRate', [0 0], ...
                           'weightDecay', [0 0]) ;
net = add_block(net, opts, '6_1', 3, 3, 64, 64, 1, 1) ;
net = add_block(net, opts, '6_2', 3, 3, 64, 32, 1, 1) ;  

% fc layer                       
net = add_block(net, opts, '7', 1, 1, 32, 32, 1, 0) ;
net = add_dropout(net, opts, '7'); 

% prediction scores layer
net = add_block(net, opts, '8', 1, 1, 32, opts.numClass, 1, 0) ;
net.layers(end) = [] ;


if opts.batchNormalization, net.layers(end) = [] ; end