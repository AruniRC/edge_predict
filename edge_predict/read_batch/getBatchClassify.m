function inputs = getBatchClassify( opts, imdb, batch )
%GETBATCHCLASSIFY Returns batches of images and labels for classification
%   Detailed explanation goes here

% opts.imSize;
% opts.averageImage;
% opts.numGpus;


inputMap = cell(1, numel(batch));

fn = imdb.images.name(batch);

for ii = 1:numel(batch)
    
   % load image
   try
        im = imread(fullfile(imdb.imageDir, fn{ii}));       
   catch ME
       disp(ME);
       continue;
   end
   
   im = imresize(im, [opts.imSize opts.imSize]);  
   
   % pre-process image
   im = single(im);
   % im = single(im);
   
   if ~isempty(opts.averageImage)
       im = bsxfun(@minus, im, opts.averageImage) ;
   end
   
   if numel(size(im)) ~= 3
      im = cat(3, {im, im, im}); 
      im = cat(3, im{:});
   end

   inputMap{ii} = im; 
end


images = single(cat(4, inputMap{:}));
labels = imdb.images.label(batch) ;

if opts.numGpus > 0
  images = gpuArray(images) ;
end

inputs = {'input', images, 'label', labels} ;

