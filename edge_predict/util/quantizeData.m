% -------------------------------------------------------------------------
function quantizedData = quantizeData(inputData, t)
% -------------------------------------------------------------------------

% initialized to zeros
quantizedData = zeros(size(inputData), 'single');

% check HOG response value bounds
inputData(inputData < t(1)) = t(1);     % bound the min value
inputData(inputData > t(end)) = t(end); % bound the max value

for ii = 1:(numel(t)-1)
    quantizedData(inputData >= t(ii) & inputData <= t(ii+1)) = ii;  
end

quantizedData(quantizedData==0) = ii;


% check that all locations are assigned a value
assert(sum(quantizedData(:) == 0)==0);