function show_hog_predictions(data, outputQuantized, imageNetDir, imFn)
% Debug utility to check correspondence between image and HOG in batches
%  To be called from the `getHogEdgeBatch()` function.
%
%   data                Loaded from MAT file, HxWx9 single HOG feature
%   outputQuantized     The ground-truth label image from 9th HOG filter
%   imageNetDir         Parent directory of original ImageNet images
%   filename            Filename of JPEG file


% visualize HOG direction map
feat_1 = repmat(data, [1 1 4]);
imhog_1 = vl_hog('render', feat_1, 'verbose', 'variant', 'dalaltriggs') ;
vl_tightsubplot(1,4,1); imshow(imhog_1); title('whole HOG'); axis('square');

% 9th HOG filter response
hog_9 = data(:,:,9);
vl_tightsubplot(1,4,2); imagesc(hog_9); title('9th HOG'); axis('square');

% resized and quantized into bins (bin-labels)
vl_tightsubplot(1,4,3); imagesc(outputQuantized); title('9th HOG quantized'); axis('square');

% original image
img = imread(fullfile(imageNetDir, imFn));
vl_tightsubplot(1,4,4); imagesc(img); title('original image'); axis('square');