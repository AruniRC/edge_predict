%--------------------------------------------------------------------------
function save_hog_response(imgDir, imgNames, outDir, cellSize)
%--------------------------------------------------------------------------
% imgDir = '/data/arunirc/datasets/ImageNet/images';
% cellSize = 8;

parfor ii = 1:length(imgNames)
    fprintf('Image %d of %d: %s\n', ii, length(imgNames), imgNames{ii});
    try
        info = imfinfo(fullfile(imgDir,imgNames{ii}));

        if ~isequal(info.Format, 'jpg')
            % skip non-JPG images, like the CMYK in ImageNet
            continue;
        end   

        img = imread(fullfile(imgDir,imgNames{ii}));
    catch ME
       ME
       continue; 
    end    
    
    % resize image
    img = imresize(img, [224 224]);
    
    % compute HOG, keep only the first normalization scheme (out of 4)
    hog = vl_hog(im2single(img), cellSize, 'variant', 'dalaltriggs') ;
    edgeSet = hog(:,:,1:9);
    
    [setDir,imName, ext] = fileparts(imgNames{ii});
    vl_xmkdir(fullfile(outDir,setDir));
    tmp_save(fullfile(outDir,setDir,imName), edgeSet);
end


%--------------------------------------------------------------------------
function tmp_save(outPath, edgeSet) 
%--------------------------------------------------------------------------
save(outPath, 'edgeSet');

