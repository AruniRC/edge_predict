% -------------------------------------------------------------------------
function outData = load_data_subset(imdb, batch)
% -------------------------------------------------------------------------
fn = imdb.images.name(batch);
outputMap = cell(1, numel(batch));
outData = cell(1, numel(batch));
for ii = 1:numel(batch)
   dat = load(fullfile(imdb.imageDir, fn{ii}));
   data = dat.edgeSet; 
   outData{ii} = data;
end