% -------------------------------------------------------------------------
function inputDataResized = resizeEdgeMaps(inputData, inputSize)
% -------------------------------------------------------------------------
inputDataResized = zeros([inputSize inputSize size(inputData,3)], ...
                                                                'single');
for jj = 1:size(inputData,3);
   % always use 'bilinear' when resizing  ('bicubic' gives -ve values)
   inputDataResized(:,:,jj) = imresize(inputData(:,:,jj), ...
                                       [inputSize inputSize], 'bilinear');
end
% inputDataResized(inputDataResized<0) = 0;
% inputDataResized(inputDataResized>0.2) = 0.2;