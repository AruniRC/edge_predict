function [net, info] = cnn_edge_train(varargin)
%CNN_MNIST  Demonstrates MatConvNet on MNIST
% 
% run(fullfile(fileparts(mfilename('fullpath')),...
%   '..', '..', 'matlab', 'vl_setupnn.m')) ;

opts.batchNormalization = false ;
opts.networkType = 'simplenn' ;
opts.modelType = 'softmax-4';
opts.baseNetwork = [];
opts.expDir = [];
opts.imdbPath = [];
opts.dataset = 'imagenet';
opts.dataDir = [];
opts.train = [];
opts.stdNorm = false;
opts.inputFilterID = [81:95];
opts.outputFilterID = 96;
opts.quantizeOutput = true;
opts.quantizeInputs = true;
opts.doAbs = true;
opts.numClasses = 200;
opts.occlusionType = 'partial-border';
[opts, varargin] = vl_argparse(opts, varargin) ;

sfx = opts.networkType ;
if opts.batchNormalization, sfx = [sfx '-bnorm'] ; end
if isfield(opts, 'expDir')
    if isempty(opts.expDir)
        opts.expDir = fullfile(vl_rootnn, 'data', ['mnist-baseline-' sfx]) ;
    end
end
[opts, varargin] = vl_argparse(opts, varargin) ;

opts.dataDir = fullfile(vl_rootnn, 'data', 'mnist') ;

if isfield(opts, 'imdbPath')
    if isempty(opts.imdbPath)
        opts.imdbPath = fullfile(opts.expDir, 'imdb.mat');
    end
end

if isfield(opts, 'train')
    if isempty(opts.train)
        opts.train = struct() ;
    end
end


opts = vl_argparse(opts, varargin) ;
if ~isfield(opts.train, 'gpus'), opts.train.gpus = []; end;




% --------------------------------------------------------------------
%                                                         Prepare data
% --------------------------------------------------------------------
% cnn_mnist_init
switch opts.modelType
   
    case 'softmax-4'
        net = cnn_edge_init('batchNormalization', opts.batchNormalization, ...
                     'networkType', opts.networkType, ...
                     'model', 'edge-net') ;               
        
    case 'alexnet-filter'
        net = cnn_edge_init('batchNormalization', opts.batchNormalization, ...
                     'networkType', opts.networkType, ...
                     'model', 'alex-filter-net', ...
                     'numChannel', numel(opts.inputFilterID)) ; 

    case 'rectangle-filter'
        net = cnn_edge_init('batchNormalization', opts.batchNormalization, ...
                     'networkType', opts.networkType, ...
                     'model', 'rectangle-filter', ...
                     'numChannel', 1) ;

    case 'edge-occlusion'  
        net = cnn_edge_init('batchNormalization', opts.batchNormalization, ...
                     'networkType', opts.networkType, ...
                     'model', 'edge-occlusion', ...
                     'numChannel', 1) ;  


    case 'edge-occlusion-wider'  
        net = cnn_edge_init('batchNormalization', opts.batchNormalization, ...
                     'networkType', opts.networkType, ...
                     'model', 'edge-occlusion-wider', ...
                     'numChannel', 1) ;   
    
                 
    % ---------------------------------------------------------------------             
    case 'gradient-filter'
        net = cnn_edge_init('batchNormalization', opts.batchNormalization, ...
                     'networkType', opts.networkType, ...
                     'model', 'gradient-filter', ...
                     'numChannel', 1) ;
                 
                 
    case 'ft-classifier-stage-1'
        % update only the first two layers and final classification weights
        %   -- only the newly inserted or modified layers are to be trained
        
        assert(~isempty(opts.baseNetwork));
        net = getNetworkFinetunable(opts.baseNetwork, 'numClass', opts.numClasses);
        
        % freeze the pre-trained layers
        for ii = 1:numel(net.params)   
            
            switch net.params(ii).name
                case {'in_conv_f', 'in_conv_b', ...
                      'conv1_1f' , 'conv1_1b' , ...
                       'c_conv_f', 'c_conv_b'}
                    % retain previous learning rates
                   
                otherwise
                    % Freeze the pre-trained layers
                     net.params(ii).learningRate = 0; 
                     net.params(ii).weightDecay = 0;
            end 
            
        end
        
        % set the network training schedule
        net.meta.trainOpts.learningRate = 0.01 * net.meta.trainOpts.learningRate;
        net.meta.trainOpts.numEpochs = 50;
        
        disp('Completed classifier network stage-1 SETUP.');
        
        
    case 'ft-classifier-stage-2'
        % update the entire network, after stage-1 fine-tuning is done
        %   -- Now the newly inserted layers have "good" values, so we can
        %      start training all the layers.
        
        % load the network
        %   -- Note: this network should be the saved model from "stage-2"
        assert(~isempty(opts.baseNetwork));
        net = dagnn.DagNN.loadobj(opts.baseNetwork); 
        
        
        % set learning rates of frozen layers
        for ii = 1:numel(net.params) 
            
            switch net.params(ii).name
                case {'in_conv_f', 'in_conv_b', ...
                       'c_conv_f', 'c_conv_b'}
                    % retain prior learning rates
                   
                otherwise
                    % un-freeze the pre-trained layers
                    if ndim(net.params(ii).value) == 4 
                        % filters have learning rate multiplier 1
                        net.params(ii).learningRate = 1; 
                        net.params(ii).weightDecay = 1;
                        
                    elseif ndim(net.params(ii).value) == 2
                        % biases have learning rate multiplier 2
                        net.params(ii).learningRate = 2; 
                        net.params(ii).weightDecay = 0;
                        
                    end 
                    
            end    
        end
        
        % set the network training schedule
        net.meta.trainOpts.learningRate = 0.01 * net.meta.trainOpts.learningRate;
        net.meta.trainOpts.numEpochs = 50;
        
        disp('Done network setup.');
        
    case 'classifier-scratch'
        % Start with a randomly initialized network with the same structure
        % as the 'gradient-filter' pre-trained network. Then perform the
        % same modifications to make it suitable for training on a
        % classification task. This acts as a baseline to see if the
        % gradient-edge-prediction pre-training actual helps over a
        % randomly initialized network, with identical architecture,
        % trained from scratch on the same task.
        
        % get the gradient-filter prediction network
        net = cnn_edge_init('batchNormalization', opts.batchNormalization, ...
                     'networkType', opts.networkType, ...
                     'model', 'gradient-filter', ...
                     'numChannel', 1) ;
        
        % modify it for classification task         
        net = getNetworkFinetunable(net, 'numClass', opts.numClasses);  
        
        % set learning rates to be the same
        for ii = 1:numel(net.params)
           if ndim(net.params.value) == 4 
                % filters have learning rate multiplier 1
                net.params.learningRate = 1; 
           elseif ndim(net.params.value) == 2
                % bias have learning rate multiplier 2
                net.params.learningRate = 2; 
           end
        end
        
    % ---------------------------------------------------------------------   
        
end
 


% for ii = 1:length(net.vars)
%     net.vars(ii).precious = true;
% end
% print(net, {'input', [150 150 1], 'label', [75 75 1]}, 'format', 'dot')
%                  
if exist(opts.imdbPath, 'file')
  imdb = load(opts.imdbPath) ;
else
  imdb = edge_get_imdb(opts.dataDir);
  mkdir(opts.expDir) ;
  save(opts.imdbPath, '-struct', 'imdb') ;
end

net.meta.classes.name = imdb.classes.name  ;
imdb.meta.classes = imdb.classes.name ;
imdb.meta.inUse = true(1,numel(imdb.meta.classes)) ;




% --------------------------------------------------------------------
%                                                                Train
% --------------------------------------------------------------------

switch opts.networkType
  case 'simplenn', trainfn = @cnn_train ;
  case 'dagnn', trainfn = @cnn_train_dag ;
end

[net, info] = trainfn(net, imdb, getBatch(opts), ...
  'expDir', opts.expDir, ...
  net.meta.trainOpts, ...
  opts.train, ...
  'val', find(imdb.images.set == 3)) ;





% --------------------------------------------------------------------
function fn = getBatch(opts)
% --------------------------------------------------------------------
switch lower(opts.networkType)
  
  case 'simplenn'
    error('Only DAG for HOG edge network.');
    
  case 'dagnn'      
    bopts = struct('numGpus', numel(opts.train.gpus)) ;
    
    switch opts.modelType
        case 'softmax-4'
            fn = @(x,y) getHogEdgeBatch(bopts,x,y) ;
            if opts.stdNorm
                bopts.stdNorm = opts.stdNorm;
                fn = @(x,y) getHogEdgeBatch(bopts,x,y) ;
            end
            
        case 'L2'
            % TODO
            
        case 'alexnet-filter'
            bopts.inputFilterID = opts.inputFilterID ;
            bopts.outputFilterID = opts.outputFilterID ;
            bopts.quantizeOutput = opts.quantizeOutput ;
            bopts.doAbs = opts.doAbs ;
            fn = @(x,y) getCNNFilterBatch(bopts,x,y) ;   
            
        case 'rectangle-filter'
            bopts.doAbs = true ;
            bopts.quantizeOutput = opts.quantizeOutput ;
            bopts.quantizeInputs = opts.quantizeInputs ;
            fn = @(x,y) getRectangleEdgeBatch(bopts,x,y) ; 
            
        case {'edge-occlusion', 'edge-occlusion-wider'}
            bopts.edgeEnergy = ~(opts.quantizeInputs) ;
            bopts.method = opts.occlusionType;
            bopts.imageSize = 100;
            fn = @(x,y) getOcclusionEdgeBatch(bopts,x,y) ;

        case 'gradient-filter'    
            bopts.doAbs = true ;
            bopts.quantizeOutput = opts.quantizeOutput ;
            bopts.quantizeInputs = opts.quantizeInputs ;
            fn = @(x,y) getRectangleEdgeBatch(bopts,x,y) ;
            
            
        case {'ft-classifier-stage-1', 'ft-classifier-stage-2', ...
                'classifier-scratch'}
            % getBatch() for fine-tuning on classification datasets
            switch opts.dataset
                
                case {'cub', 'cars'}
                    bopts.imSize = 150 ;
                    bopts.averageImage = [] ;
                    bopts.numGpus = numel(opts.train.gpus) ;
                    fn = @(x,y) getBatchClassify(bopts,x,y) ;         
                    
                case 'svhn'
                    
                    
                case 'cifar-10'
                    
                
            end
        
            
        otherwise
            error('Invalid MODELTYPE value for opts.modelType.');
            
    end
end

