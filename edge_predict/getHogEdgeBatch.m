% -------------------------------------------------------------------------
function inputs = getHogEdgeBatch(opts, imdb, batch)
% -------------------------------------------------------------------------

% inputSize = 224;
% labelSize = 56;
% t = linspace(0, 0.2, 5);
% t = [0,   0.0410,    0.0980,    0.1698,    0.2];  % percentile bins - 100 val
t = [0,   0.0393,    0.0954,    0.1690,    0.2];  % percentile bins - 1000 train
fn = imdb.images.name(batch);

DEBUG = false;
STD_NORM = false;
% debug settings
if isfield(opts, 'debug')
    if opts.debug
        figure; 
        DEBUG = true;
        imageNetDir = '/data/arunirc/datasets/ImageNet/images';
        visDir = 'data/vis-output';  vl_xmkdir(visDir);
    end
end

% if using standard-deviation normalised HOG
if isfield(opts, 'stdNorm')
    if opts.stdNorm
        t = [0   0.0047    0.0084    0.0147   0.0211];
        STD_NORM = true;
    end
end


inputMap = cell(1, numel(batch));
outputMap = cell(1, numel(batch));

for ii = 1:numel(batch)
   dat = load(fullfile(imdb.imageDir, fn{ii}));
   data = dat.edgeSet; 
   
   if STD_NORM
       % normalize by standard deviation over orientation bins
       %    suppress HOG that have high response at all orientations
       stdDev = std(data, 0, 3); 
       data = bsxfun(@times, data, stdDev);
   end
   
   % process edge maps
   inputData = data(:,:,1:8);  % first 8 HOG edgemaps
   % inputDataResized = resizeEdgeMaps(inputData, inputSize);
   
   outputData = data(:,:,9); % last edgemap to be predicted 
   % outputDataResized = resizeEdgeMaps(outputData, labelSize);
   
   % Quantize values: make ground-truth edge magnitudes into labels
   % inputQuantized = quantizeData(inputDataResized, t);
   outputQuantized = quantizeData(outputData, t);
   
   inputMap{ii} = inputData;
   outputMap{ii} = outputQuantized;
   
   % get filename of original JPEG image
   [a,b,~] = fileparts(fn{ii});
   imFn = [a filesep b '.JPEG'];
   
   
   % display data for visual debugging
   if DEBUG
       show_hog_batch(data, outputQuantized, imageNetDir, imFn);
       unique(outputQuantized)
       
       if STD_NORM
          saveas(gca, fullfile(visDir, [num2str(ii) '-stdNorm.png']));
       else
          saveas(gca, fullfile(visDir, [num2str(ii) '.png']));
       end
       clc;
       pause(2);
   end
end

images = single(cat(4, inputMap{:}));
labels = single(cat(4, outputMap{:}));

assert(numel(unique(labels(:)))==(numel(t)-1));


if opts.numGpus > 0
  images = gpuArray(images) ;
end
inputs = {'input', images, 'label', labels} ;

