
%% Initial settings

expDir = 'data/edge_alexnet_v1';
dataDir = '/data/arunirc/datasets/ImageNet-alexnet-filter/';
rng(0);


%% Make IMDB

if ~exist(fullfile(expDir, 'imdb', 'imdb-cnn.mat'), 'file')  
    imdb = edge_get_imdb(dataDir);
    vl_xmkdir(fullfile(expDir, 'imdb'));
    save(fullfile(expDir, 'imdb', 'imdb-cnn.mat'), '-struct', 'imdb');
end


%% Train the network for HOG

[net, info] = cnn_edge_train(...
                      'baseNetwork', [], ...
                      'expDir', fullfile(expDir, 'cnn-train'), ...
                      'batchNormalization', true, ...
                      'imdbPath', fullfile(expDir, 'imdb', 'imdb-cnn.mat'), ...
                      'networkType', 'dagnn', ...
                      'modelType', 'alexnet-filter', ...
                      'train', struct('gpus', [2]));


% save network
if ~exist(fullfile(expDir, 'cnn_model', 'net_occlusion.mat'), 'file')
    vl_xmkdir(fullfile(expDir, 'cnn_model')); s = saveobj(net);
    save(fullfile(expDir, 'cnn_model', 'net_occlusion.mat'), '-struct', 's');
end




