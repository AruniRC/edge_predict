% run_vis_rect_predict


%% 0) Initial settings and paths

setup;
% expDir = 'data/edge_rect_v1.3.1';
expDir = 'data/edge_gradient_v3';
imdbPath = fullfile(expDir, 'imdb', 'imdb-cnn.mat')'; 
epoch = 20;  % load network at different epochs
netPath = fullfile(expDir, sprintf('cnn-train/net-epoch-%d.mat', epoch));   % specify which network to use


%% 1) Load network
s = load(netPath);
net = dagnn.DagNN.loadobj(s.net);
% net = cnn_imagenet_deploy(net);
% net.removeLayer('prob');  % binary loss

%% 2) Evaluate network on subset of dataset


% settings
imdb = load(imdbPath);
opts.numGpus = [];
opts.quantizeInput = false;
opts.edgeEnergy = false;
opts.method = 'gradient-filter';
opts.imageSize = 150;
opts.quantizeOutput = false;
% opts.quantizeInput = false;
opts.doAbs = true;


%%
rng(0);

% grab images from validation set
batch = find(imdb.images.set == 2); % or TRAIN
batch = batch(randperm(length(batch), 20));

% inputs = getRectangleEdgeBatch(opts, imdb, batch);
inputs = getRectangleEdgeBatch_temp(opts, imdb, batch);


% set GPU mode ('cpu'/'gpu') and evaluate network
net.mode = 'test';
net.move('cpu');
net.eval(inputs(1:2));


% ground-truth labels
gtLabels = gather(inputs{4});

% network predictions
% -- BINARY!!!
scores = gather(net.vars(net.getVarIndex('prediction')).value);
prob = vl_nnsigmoid(scores) ;

inputEdgeMaps = gather(inputs{2});


%% 3) Visualize label histograms

%{
figure;

subplot(1,2,1);
gtLabelVec = gtLabels(:);
histogram(gtLabelVec);
title('gt');

subplot(1,2,2);
probVec = prob(:);
histogram(probVec);
title('probs');
%}


%% 4) Visualize images and predictions

% imageNetDir = '/data/arunirc/datasets/ImageNet/images';
visDir = fullfile(expDir, 'cnn-viz-preds-train');  
vl_xmkdir(visDir);

figure;
fn = imdb.images.name(batch);

for ii = 1:numel(batch)
    
   % load original image
   img = imread(fullfile(imdb.imageDir, fn{ii}));
%    
%    vl_tightsubplot(1,4,1); imagesc(img); 
%    title('img'); axis('square'); set(gca,'xtick',[]);  set(gca,'ytick',[]);
   %colorbar;
   
   % input edge map
   vl_tightsubplot(1,4,2); imagesc(inputEdgeMaps(:,:,:,ii)); 
   title('input'); axis('square'); set(gca,'xtick',[]); set(gca,'ytick',[]);
   %colorbar;
   
   
   % show ground-truth labels of missing filter
   vl_tightsubplot(1,4,3); imagesc(gtLabels(:,:,:,ii)); 
   title('gt'); axis('square'); set(gca,'xtick',[]); set(gca,'ytick',[]);
   %colorbar;
   
   % output probability heatmap
   vl_tightsubplot(1,4,4); imagesc(prob(:,:,:,ii)); 
   title('pred'); axis('square'); set(gca,'xtick',[]); set(gca,'ytick',[]); 
   %colorbar;
   
   % pause(1);
   drawnow;
   saveas(gca, fullfile(visDir, ['epoch_' num2str(epoch) 'preds_' num2str(ii) '.png']));
    
end

